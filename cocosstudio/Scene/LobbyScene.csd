<GameProjectFile>
  <PropertyGroup Type="Scene" Name="LobbyScene" ID="22f60a18-b07b-4c6b-bcb2-1cbda15bc32e" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="LobbyScene" CustomClassName="LobbyScene" Tag="5" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="LobbyBgLayer" ActionTag="-345041260" Tag="71" IconVisible="True" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Layer/Lobby/LobbyBgLayer.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="LobbyBottonTabLayer" ActionTag="-1882669690" Tag="96" IconVisible="True" TopMargin="860.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="640.0000" Y="100.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.1042" />
            <FileData Type="Normal" Path="Layer/Lobby/LobbyBottomTabLayer.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BottomFrame" ActionTag="1301820302" Tag="94" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="297.0000" RightMargin="297.0000" TopMargin="960.0000" BottomMargin="-46.0000" ctype="SpriteObjectData">
            <Size X="2048.0000" Y="188.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="320.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Img/bottom_frame.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="TopFrame" ActionTag="974086149" Tag="95" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="297.0000" RightMargin="297.0000" TopMargin="-46.0000" BottomMargin="960.0000" ctype="SpriteObjectData">
            <Size X="2048.0000" Y="188.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="320.0000" Y="960.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Img/top_frame.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_4" ActionTag="1455871786" Tag="258" IconVisible="True" LeftMargin="0.0081" RightMargin="-0.0081" BottomMargin="810.0000" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="640.0000" Y="150.0000" />
            <AnchorPoint />
            <Position X="0.0081" Y="810.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.8438" />
            <PreSize X="1.0000" Y="0.1563" />
            <FileData Type="Normal" Path="Layer/Lobby/LobbyTopTabLayer.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>