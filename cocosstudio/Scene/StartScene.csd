<GameProjectFile>
  <PropertyGroup Type="Scene" Name="StartScene" ID="fcd40db4-2674-4b61-af1c-301a5a56b22f" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="StartScene" CustomClassName="StartScene" Tag="4" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="-1714230769" Tag="38" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" TopMargin="-88.0000" BottomMargin="-88.0000" Scale9Width="640" Scale9Height="1136" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="1135.9680" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.1833" />
            <FileData Type="Normal" Path="Img/background.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="LoadingBar_1" ActionTag="514403052" Tag="11" IconVisible="False" LeftMargin="120.0000" RightMargin="120.0000" TopMargin="821.8579" BottomMargin="118.1421" ProgressInfo="71" ctype="LoadingBarObjectData">
            <Size X="400.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="128.1421" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1335" />
            <PreSize X="0.6250" Y="0.0208" />
            <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>