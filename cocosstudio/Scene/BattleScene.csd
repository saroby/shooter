<GameProjectFile>
  <PropertyGroup Type="Scene" Name="BattleScene" ID="b21e8aa6-73f4-4e05-8672-a78a835aaef6" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="BattleScene" CustomClassName="BattleScene" Tag="3" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="BottomFrame" ActionTag="-1056288266" Tag="28" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="297.0000" RightMargin="297.0000" TopMargin="960.0000" BottomMargin="-46.0000" ctype="SpriteObjectData">
            <Size X="2048.0000" Y="188.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="320.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Img/bottom_frame.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_2" ActionTag="1792502556" Tag="29" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="297.0000" RightMargin="297.0000" TopMargin="-46.0000" BottomMargin="960.0000" ctype="SpriteObjectData">
            <Size X="2048.0000" Y="188.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="320.0000" Y="960.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Img/top_frame.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>