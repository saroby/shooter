<GameProjectFile>
  <PropertyGroup Type="Layer" Name="WebNoticePopupLayer" ID="ef2084e3-6bf8-49a8-aaa3-24e3119f4b59" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="WebNoticePopupLayer" CustomClassName="WebNoticePopupLayer" Tag="37" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="1151277504" Tag="39" IconVisible="False" Scale9Enable="True" LeftEage="595" RightEage="595" TopEage="356" BottomEage="356" Scale9OriginX="595" Scale9OriginY="356" Scale9Width="614" Scale9Height="368" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="Img/web_notice_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ScrollView_1" ActionTag="-1305512680" Tag="38" IconVisible="False" LeftMargin="20.0000" RightMargin="20.0000" TopMargin="20.0000" BottomMargin="133.0000" TouchEnable="True" ComboBoxIndex="1" ColorAngle="90.0000" IsBounceEnabled="True" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="600.0000" Y="807.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="320.0000" Y="133.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1385" />
            <PreSize X="0.9375" Y="0.8406" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="600" Height="807" />
          </AbstractNodeData>
          <AbstractNodeData Name="CheckBox_1" ActionTag="-111893451" Tag="40" IconVisible="False" LeftMargin="29.3254" RightMargin="530.6746" TopMargin="850.7196" BottomMargin="29.2804" TouchEnable="True" CheckedState="True" ctype="CheckBoxObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="69.3254" Y="69.2804" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1083" Y="0.0722" />
            <PreSize X="0.1250" Y="0.0833" />
            <NormalBackFileData Type="Default" Path="Default/CheckBox_Normal.png" Plist="" />
            <PressedBackFileData Type="Default" Path="Default/CheckBox_Press.png" Plist="" />
            <DisableBackFileData Type="Default" Path="Default/CheckBox_Disable.png" Plist="" />
            <NodeNormalFileData Type="Default" Path="Default/CheckBoxNode_Normal.png" Plist="" />
            <NodeDisableFileData Type="Default" Path="Default/CheckBoxNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Button_1" ActionTag="2136985459" CallBackType="Click" Tag="41" IconVisible="False" LeftMargin="220.0000" RightMargin="220.0000" TopMargin="850.7100" BottomMargin="29.2900" TouchEnable="True" FontSize="14" ButtonText="확인" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="69.2900" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="165" B="0" />
            <PrePosition X="0.5000" Y="0.0722" />
            <PreSize X="0.3125" Y="0.0833" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>