<GameProjectFile>
  <PropertyGroup Type="Layer" Name="AttendancePopupLayer" ID="d4beb762-5b3e-4495-a5d2-173ee391dadb" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="150" Speed="1.0000" ActivedAnimationName="show">
        <Timeline ActionTag="720027966" Property="Position">
          <PointFrame FrameIndex="100" X="321.8595" Y="52.8149">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="720027966" Property="Scale">
          <ScaleFrame FrameIndex="100" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="720027966" Property="RotationSkew">
          <ScaleFrame FrameIndex="100" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="720027966" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="2" />
          </IntFrame>
          <IntFrame FrameIndex="50" Value="255">
            <EasingData Type="2" />
          </IntFrame>
          <IntFrame FrameIndex="100" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="150" Value="0">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
        <Timeline ActionTag="-1019673807" Property="Alpha">
          <IntFrame FrameIndex="0" Value="0">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="50" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="100" Value="255">
            <EasingData Type="0" />
          </IntFrame>
          <IntFrame FrameIndex="150" Value="0">
            <EasingData Type="0" />
          </IntFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="show" StartIndex="0" EndIndex="50">
          <RenderColor A="255" R="178" G="34" B="34" />
        </AnimationInfo>
        <AnimationInfo Name="idle" StartIndex="50" EndIndex="100">
          <RenderColor A="255" R="248" G="248" B="255" />
        </AnimationInfo>
        <AnimationInfo Name="hide" StartIndex="100" EndIndex="150">
          <RenderColor A="255" R="255" G="0" B="255" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="AttendancePopupLayer" CustomClassName="AttendancePopupLayer" Tag="42" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="close_btn" ActionTag="720027966" CallBackType="Click" CallBackName="close" Tag="45" IconVisible="False" LeftMargin="221.8595" RightMargin="218.1405" TopMargin="877.1851" BottomMargin="22.8149" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="321.8595" Y="52.8149" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="224" G="192" B="120" />
            <PrePosition X="0.5029" Y="0.0550" />
            <PreSize X="0.3125" Y="0.0625" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="main_sprite" ActionTag="-1019673807" Tag="67" IconVisible="False" LeftMargin="-2.1198" RightMargin="3.1198" TopMargin="16.6249" BottomMargin="93.3751" Scale9Width="639" Scale9Height="850" ctype="ImageViewObjectData">
            <Size X="639.0000" Y="850.0000" />
            <Children>
              <AbstractNodeData Name="Node_25" ActionTag="953289280" Tag="54" IconVisible="True" LeftMargin="540.4747" RightMargin="98.5253" TopMargin="683.3676" BottomMargin="166.6324" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="540.4747" Y="166.6324" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8458" Y="0.1960" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_24" ActionTag="1698045632" Tag="53" IconVisible="True" LeftMargin="430.6256" RightMargin="208.3744" TopMargin="683.3676" BottomMargin="166.6324" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="430.6256" Y="166.6324" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6739" Y="0.1960" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_23" ActionTag="1619106016" Tag="52" IconVisible="True" LeftMargin="312.1025" RightMargin="326.8975" TopMargin="683.3676" BottomMargin="166.6324" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="312.1025" Y="166.6324" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4884" Y="0.1960" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_22" ActionTag="1644587129" Tag="51" IconVisible="True" LeftMargin="205.1439" RightMargin="433.8561" TopMargin="683.3676" BottomMargin="166.6324" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="205.1439" Y="166.6324" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3210" Y="0.1960" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_21" ActionTag="1833116667" Tag="50" IconVisible="True" LeftMargin="95.2939" RightMargin="543.7061" TopMargin="683.3676" BottomMargin="166.6324" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="95.2939" Y="166.6324" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1491" Y="0.1960" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_20" ActionTag="178103494" Tag="49" IconVisible="True" LeftMargin="543.9856" RightMargin="95.0144" TopMargin="551.9267" BottomMargin="298.0733" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="543.9856" Y="298.0733" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8513" Y="0.3507" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_19" ActionTag="-576543486" Tag="48" IconVisible="True" LeftMargin="444.2546" RightMargin="194.7454" TopMargin="551.9267" BottomMargin="298.0733" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="444.2546" Y="298.0733" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6952" Y="0.3507" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_18" ActionTag="296803689" Tag="47" IconVisible="True" LeftMargin="322.8403" RightMargin="316.1597" TopMargin="551.9267" BottomMargin="298.0733" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="322.8403" Y="298.0733" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5052" Y="0.3507" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_17" ActionTag="-2011407962" Tag="46" IconVisible="True" LeftMargin="211.5460" RightMargin="427.4540" TopMargin="551.9267" BottomMargin="298.0733" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="211.5460" Y="298.0733" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3311" Y="0.3507" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_16" ActionTag="-1896039918" Tag="45" IconVisible="True" LeftMargin="95.9135" RightMargin="543.0865" TopMargin="551.9267" BottomMargin="298.0733" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="95.9135" Y="298.0733" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1501" Y="0.3507" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_15" ActionTag="-1314916041" Tag="44" IconVisible="True" LeftMargin="540.4747" RightMargin="98.5253" TopMargin="438.3739" BottomMargin="411.6261" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="540.4747" Y="411.6261" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8458" Y="0.4843" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_14" ActionTag="-1384452172" Tag="43" IconVisible="True" LeftMargin="421.9535" RightMargin="217.0465" TopMargin="442.7103" BottomMargin="407.2897" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="421.9535" Y="407.2897" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6603" Y="0.4792" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_13" ActionTag="357828040" Tag="42" IconVisible="True" LeftMargin="313.5478" RightMargin="325.4522" TopMargin="438.3739" BottomMargin="411.6261" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="313.5478" Y="411.6261" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4907" Y="0.4843" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_12" ActionTag="1697427673" Tag="41" IconVisible="True" LeftMargin="203.6979" RightMargin="435.3021" TopMargin="439.8199" BottomMargin="410.1801" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="203.6979" Y="410.1801" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3188" Y="0.4826" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_11" ActionTag="1713134313" Tag="40" IconVisible="True" LeftMargin="92.4034" RightMargin="546.5966" TopMargin="441.2643" BottomMargin="408.7357" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="92.4034" Y="408.7357" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1446" Y="0.4809" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_10" ActionTag="-318679654" Tag="39" IconVisible="True" LeftMargin="539.0311" RightMargin="99.9689" TopMargin="316.9619" BottomMargin="533.0381" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="539.0311" Y="533.0381" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8436" Y="0.6271" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_9" ActionTag="620617368" Tag="38" IconVisible="True" LeftMargin="427.7345" RightMargin="211.2655" TopMargin="316.9619" BottomMargin="533.0381" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="427.7345" Y="533.0381" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6694" Y="0.6271" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_8" ActionTag="31445040" Tag="37" IconVisible="True" LeftMargin="312.1025" RightMargin="326.8975" TopMargin="314.0707" BottomMargin="535.9293" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="312.1025" Y="535.9293" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4884" Y="0.6305" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_7" ActionTag="1043056317" Tag="36" IconVisible="True" LeftMargin="206.5892" RightMargin="432.4108" TopMargin="318.4080" BottomMargin="531.5920" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="206.5892" Y="531.5920" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3233" Y="0.6254" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_6" ActionTag="132766004" Tag="35" IconVisible="True" LeftMargin="93.8487" RightMargin="545.1513" TopMargin="321.2983" BottomMargin="528.7017" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="93.8487" Y="528.7017" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1469" Y="0.6220" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_5" ActionTag="222016700" Tag="34" IconVisible="True" LeftMargin="539.0311" RightMargin="99.9689" TopMargin="191.2119" BottomMargin="658.7881" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="539.0311" Y="658.7881" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8436" Y="0.7750" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_4" ActionTag="928796148" Tag="33" IconVisible="True" LeftMargin="426.2899" RightMargin="212.7101" TopMargin="192.6572" BottomMargin="657.3428" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="426.2899" Y="657.3428" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6671" Y="0.7733" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_3" ActionTag="1245720865" Tag="32" IconVisible="True" LeftMargin="317.8842" RightMargin="321.1158" TopMargin="191.2140" BottomMargin="658.7860" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="317.8842" Y="658.7860" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4975" Y="0.7750" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_2" ActionTag="28999276" Tag="31" IconVisible="True" LeftMargin="208.0344" RightMargin="430.9656" TopMargin="189.7683" BottomMargin="660.2317" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="208.0344" Y="660.2317" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3256" Y="0.7767" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Node_1" ActionTag="-596093388" Tag="30" IconVisible="True" LeftMargin="89.6868" RightMargin="549.3132" TopMargin="189.9420" BottomMargin="660.0580" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="89.6868" Y="660.0580" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1404" Y="0.7765" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bottom_notice_text" ActionTag="-1870371678" Tag="55" IconVisible="False" LeftMargin="122.5000" RightMargin="121.5000" TopMargin="745.0000" BottomMargin="85.0000" FontSize="20" LabelText="수령한 보상은 우편함에서 확인 가능합니다." HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="395.0000" Y="20.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="320.0000" Y="95.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5008" Y="0.1118" />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="317.3802" Y="518.3751" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4959" Y="0.5400" />
            <PreSize X="0.9984" Y="0.8854" />
            <FileData Type="Normal" Path="Img/bg_attendance.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>