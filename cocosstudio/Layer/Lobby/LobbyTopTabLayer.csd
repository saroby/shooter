<GameProjectFile>
  <PropertyGroup Type="Layer" Name="LobbyTopTabLayer" ID="901679a3-c79c-43be-97af-06e0352a5ce1" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="LobbyTopTabLayer" CustomClassName="LobbyTopTabLayer" Tag="188" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="150.0000" />
        <Children>
          <AbstractNodeData Name="ProjectNode_1" ActionTag="-477930643" Tag="189" IconVisible="True" LeftMargin="62.6907" RightMargin="577.3093" TopMargin="66.3883" BottomMargin="83.6117" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="62.6907" Y="83.6117" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0980" Y="0.5574" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Node/CharacterStateNode.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_2" ActionTag="-836585752" Tag="196" IconVisible="True" LeftMargin="176.8867" RightMargin="463.1133" TopMargin="66.3883" BottomMargin="83.6117" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="176.8867" Y="83.6117" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2764" Y="0.5574" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Node/CharacterStateNode.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProjectNode_3" ActionTag="1635775100" Tag="203" IconVisible="True" LeftMargin="291.0814" RightMargin="348.9186" TopMargin="66.3883" BottomMargin="83.6117" InnerActionSpeed="1.0000" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="291.0814" Y="83.6117" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4548" Y="0.5574" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Node/CharacterStateNode.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>