<GameProjectFile>
  <PropertyGroup Type="Node" Name="CharacterStateNode" ID="f7cbed0e-4035-4cf8-b720-1294eddc1e10" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="CharacterStateNode" Tag="128" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="-617465295" Tag="129" IconVisible="False" LeftMargin="-40.0000" RightMargin="-40.0000" TopMargin="-40.0000" BottomMargin="-40.0000" ctype="SpriteObjectData">
            <Size X="80.0000" Y="80.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Img/light_skill_1_1_small.jpg" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="card_btn_small_nomark_1_3" ActionTag="-374198714" Tag="135" IconVisible="False" LeftMargin="-57.0000" RightMargin="23.0000" TopMargin="23.0000" BottomMargin="-57.0000" ctype="SpriteObjectData">
            <Size X="114.0000" Y="114.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="Img/card_btn_small_nomark_1.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="LoadingBar_1" ActionTag="-1184483452" Tag="130" IconVisible="False" LeftMargin="-38.8553" RightMargin="-41.1447" TopMargin="51.1783" BottomMargin="-61.1783" ProgressInfo="100" ctype="LoadingBarObjectData">
            <Size X="80.0000" Y="10.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="1.1447" Y="-56.1783" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="128" B="0" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Lv" ActionTag="-1051459646" Tag="136" IconVisible="False" LeftMargin="-81.0000" RightMargin="-81.0000" TopMargin="-47.9534" BottomMargin="11.9534" LabelText="Lv.10" ctype="TextBMFontObjectData">
            <Size X="63.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position Y="29.9534" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Default" Path="Default/defaultBMFont.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_3" ActionTag="-957239515" Tag="137" IconVisible="False" LeftMargin="-82.7698" RightMargin="-79.2302" TopMargin="-27.1671" BottomMargin="-8.8329" LabelText="Character Name" ctype="TextBMFontObjectData">
            <Size X="184.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-1.7698" Y="9.1671" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Default" Path="Default/defaultBMFont.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>