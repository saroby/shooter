#include "ShooterPch.h"
#include "AppDelegate.h"
#include "Helper/Config.h"
#include "UI/UIManager.h"

USING_NS_CC;
using namespace UI;

static cocos2d::Size designResolutionSize = cocos2d::Size(640, 960);
static cocos2d::Size smallResolutionSize = cocos2d::Size(480, 320);
static cocos2d::Size mediumResolutionSize = cocos2d::Size(1024, 768);
static cocos2d::Size largeResolutionSize = cocos2d::Size(2048, 1536);

AppDelegate::AppDelegate() 
{
}

AppDelegate::~AppDelegate() 
{
	Uninit();
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages, 
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() 
{
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if (!glview) 
	{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("Shooter", Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("Shooter");
#endif
        director->setOpenGLView(glview);
    }
    
	Init();

    director->setDisplayStats(Config::Instance().use_frame_info_); // turn on display FPS
    director->setAnimationInterval(1.0 / 60); // set FPS. the default value is 1.0/60 if you don't call this

    // Set the design resolution
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::NO_BORDER);
    Size frameSize = glview->getFrameSize();
    
    if (frameSize.height > mediumResolutionSize.height)
    {	// if the frame's height is larger than the height of medium size.
        director->setContentScaleFactor(MIN(largeResolutionSize.height/designResolutionSize.height, largeResolutionSize.width/designResolutionSize.width));
    }
    else if (frameSize.height > smallResolutionSize.height)
    {	// if the frame's height is larger than the height of small size.
        director->setContentScaleFactor(MIN(mediumResolutionSize.height/designResolutionSize.height, mediumResolutionSize.width/designResolutionSize.width));
    }
    else
    {	// if the frame's height is smaller than the height of medium size.
        director->setContentScaleFactor(MIN(smallResolutionSize.height/designResolutionSize.height, smallResolutionSize.width/designResolutionSize.width));
    }

    register_all_packages();

	//S: Show start scene
	auto cmd = Cmd<UINotifyType>(UINotifyType::ShowScene);
	cmd.data_stream_->Write(UISceneType::Start);
	UIManager::Instance().Broadcast(cmd);
	//E: Show start scene

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() 
{
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() 
{
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void AppDelegate::Init()
{
	//S: Add search pathes
	FileUtils::getInstance()->addSearchPath("res");
	//E: Add search pathes

	//:S initialize singleton
	Config::Instance();
	UIManager::Instance();
	//:E initialize singleton
}

void AppDelegate::Uninit()
{
	//:S Finalize singleton
	Config::Destroy();
	UIManager::Destroy();
	//:E Finalize singleton
}
