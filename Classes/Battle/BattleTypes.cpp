#include "ShooterPch.h"
#include "BattleTypes.h"
#include "BattleDefines.h"

Battle::CardSkillData::CardSkillData()
	: no_(0)
	, grade_(0)
	, card_count_(0)
	, level_(0)
	, damage_boost_percent_(0)
	, is_today_card_(false)
	, skill_type_(SkillType::INVALID_SKILL_TYPE)
	, slot_type_(SkillSlotType::kInvalidSkillSlotType)
{

}

Battle::ScoreData::ScoreData()
{
	Init();
}

void Battle::ScoreData::Init()
{
	basic_score_ = 0;
	group_kill_score_ = 0;
	grade_score_ = 0;
	r_grade_count_ = 0;
	ss_grade_count_ = 0;
	s_grade_count_ = 0;
	a_grade_count_ = 0;
	b_grade_count_ = 0;
}

void Battle::ScoreData::ReadFromCommonVars()
{

}

void Battle::ScoreData::WriteToCommonVars()
{

}

int Battle::ScoreData::GetTotalScore() const
{
	return basic_score_ + group_kill_score_ + grade_score_;
}

void Battle::ScoreData::WriteTo(std::string& out)
{

}

void Battle::ScoreData::ReadFrom(const std::string& in)
{

}

Battle::InitData::InitData()
{

}

void Battle::InitData::SelectBackground()
{
	srand(time(NULL));

	std::string background_ids = ClientDb::GetDBAttribute("phase_set", phase_set_no_, "background_ids").ToString();

	std::vector<std::string> background_id_list = Helper::String::Split(background_ids, ",");

	int index = rand() % (int)background_id_list.size();

	background_id_ = background_id_list[index];
}