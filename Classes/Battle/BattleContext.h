#pragma once
#include "BattleDefines.h"

namespace Battle
{
	class BattlePlaygound;
	class BattleBackground;
	class BattleCoordinator;
	class BattleDropItemGenerator;
	class BattleContext
		: public Singleton<BattleContext>
	{
	public:
		BattleContext();
		~BattleContext();

		static BattlePlaygound* GetPlayground() { return Instance().playground_; };
		static BattleBackground* GetBackgrond() { return Instance().background_; };
		static BattleCoordinator* GetCoordinator() { return Instance().coordinator_; };
		static const Const& GetConst() { return Instance().const_; };
	private:
		void Init();
		void Uninit();

		BattlePlaygound* playground_;
		BattleBackground* background_;
		BattleCoordinator* coordinator_;
		Const const_;
	};
}


