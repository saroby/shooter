#pragma once
#include <vector>

namespace Battle
{
	namespace SkillType
	{
		enum Define
		{
			INVALID_SKILL_TYPE = 0,
			FIRE_SKILL_TYPE = 1 << 0,
			WATER_SKILL_TYPE = 1 << 1,
			LIGHT_SKILL_TYPE = 1 << 2,
			DUAL_PROPERTY = 1 << 3,
			FIRE_WATER_SKILL_TYPE = DUAL_PROPERTY | FIRE_SKILL_TYPE | WATER_SKILL_TYPE,
			FIRE_LIGHT_SKILL_TYPE = DUAL_PROPERTY | FIRE_SKILL_TYPE | LIGHT_SKILL_TYPE,
			WATER_LIGHT_SKILL_TYPE = DUAL_PROPERTY | WATER_SKILL_TYPE | LIGHT_SKILL_TYPE,
			ALL_SKILL_TYPE = FIRE_SKILL_TYPE | WATER_SKILL_TYPE | LIGHT_SKILL_TYPE,
		};
	}

	namespace SkillSlotType
	{
		enum Define
		{
			kInvalidSkillSlotType = -1,
			kStartSkillSlotType = 0,
			kSkillSlot1 = kStartSkillSlotType,
			kSkillSlot2 = 1,
			kSkillSlot3 = 2,
			kSkillSlotFriend = 3,
			kMaxSkillSlotType,
		};
	}

	struct CardSkillData
	{
		CardSkillData();

		int no_;
		int grade_;
		int card_count_;
		int level_;
		int damage_boost_percent_;
		bool is_today_card_;
		SkillType::Define skill_type_;
		SkillSlotType::Define slot_type_;
	};

	struct ScoreData
	{
		ScoreData();

		void Init();
		void ReadFromCommonVars();
		void WriteToCommonVars();
		int GetTotalScore() const;
		void WriteTo(std::string& out);
		void ReadFrom(const std::string& in);

		int basic_score_;
		int group_kill_score_;
		int grade_score_;
		int r_grade_count_;
		int ss_grade_count_;
		int s_grade_count_;
		int a_grade_count_;
		int b_grade_count_;
	};

	struct InitData
	{
		InitData();
		void SelectBackground();

		int preset_no_;
		int phase_set_no_;
		int adventure_stage_no_;
		int infinite_stage_no_;
		int start_phase_;
		bool show_scenario_;
		bool use_mana_enhancer_;
		bool use_unlimited_shield_;
		bool use_one_off_mana_recovery_;
		bool use_ticket_1_;
		bool use_ticket_2_;
		bool use_ticket_3_;
		bool use_ticket_4_;
		bool free_ticket_;
		bool use_system_friend_;
		int character_no_;
		int basic_weapon_no_;
		ScoreData score_data_;
		CardSkillData slot1_card_skill_data_;
		CardSkillData slot2_card_skill_data_;
		CardSkillData slot3_card_skill_data_;
		CardSkillData friend_card_skill_data_;
		std::vector<int> card_combination_no_list_;
		std::string background_id_;
	};
}