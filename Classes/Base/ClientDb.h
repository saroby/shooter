#ifndef __CLIENT_DB_H__
#define __CLIENT_DB_H__
#pragma once
#include "Singleton.h"
#include <vector>
#include <string>
#include <map>
#include "pugixml/src/pugixml.hpp"

class DBAttribute
{
public:
	DBAttribute(const pugi::xml_node& node);

	int ToInt() const;
	float ToFloat() const;
	const std::string ToString() const;
	bool ToBool() const;

private:
	pugi::xml_node node_;
};

class ClientDb
	: Singleton<ClientDb>
{
public:
	ClientDb();
	~ClientDb();
	static const pugi::xml_document& GetTable(const std::string& table_name);
	static DBAttribute GetDBAttribute(const std::string& table_name, const std::string& id, const std::string& attribute_name);
	static DBAttribute GetDBAttribute(const std::string& table_name, int no, const std::string& attribute_name);
	static DBAttribute GetDBAttribute(const std::string& table_name, const std::string& finder_attr, const std::string& finder_attr_value, const std::string& result_attr);
	static DBAttribute GetDBAttribute(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, const std::string& finder_attr2_value, const std::string& result_attr);
	static DBAttribute GetDBAttribute(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, int finder_attr2_value, const std::string& result_attr);
	static DBAttribute GetDBAttribute(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, int finder_attr2_value, const std::string& finder_attr3, int finder_attr3_value, const std::string& result_attr);
	static DBAttribute GetDBAttribute(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, const std::string& finder_attr2_value, const std::string& finder_attr3, const std::string& finder_attr3_value, const std::string& result_attr);
	static std::string ToId(const std::string& table_name, int no);
	static int ToNo(const std::string& table_name, const std::string& id);
	static bool VerifyNo(const std::string& table_name, int no);
	static bool VerifyAttribute(const std::string& table_name, const std::string& attribute_name, std::string attribute_value);
	static bool VerifyAttribute(const std::string& table_name, const std::string& attribute_name, int attribute_value);
	static int GetNo(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, int finder_attr2_value);
private:
	void Init();
	void Uninit();

	std::map<std::string, pugi::xml_document*> table_map_;
};
#endif  // __CLIENT_DB_H__