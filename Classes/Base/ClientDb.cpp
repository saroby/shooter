﻿#include "ShooterPch.h"
#include "ClientDb.h"
#include "../Helper/HelperXml.h"

using namespace pugi;

//////////////////////////////////////////////////////////////////////////

DBAttribute::DBAttribute(const xml_node& node) 
	: node_(node)
{
}

int DBAttribute::ToInt() const
{
	return node_.text().as_int();
}

float DBAttribute::ToFloat() const
{
	return node_.text().as_float();
}

const std::string DBAttribute::ToString() const
{
	return node_.text().as_string();
}

bool DBAttribute::ToBool() const
{
	return node_.text().as_bool();
}

//////////////////////////////////////////////////////////////////////////

ClientDb::ClientDb()
{
	Init();
}


ClientDb::~ClientDb()
{
	Uninit();
}

const pugi::xml_document& ClientDb::GetTable(const std::string& table_name)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	return *xml_doc;
}

DBAttribute ClientDb::GetDBAttribute(const std::string& table_name, const std::string& id, const std::string& attribute_name)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[@id='%s']", table_name.c_str(), id.c_str());
	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	CCASSERT(!node.empty(), ("db record find failed, table(%s), id(%s)", table_name.c_str(), id.c_str()));

	xml_node child_node = node.child(attribute_name.c_str());
	CCASSERT(!child_node.empty(), ("db attrib find failed, table(%s), id(%s), attrib(%s)", table_name.c_str(), id.c_str(), attribute_name.c_str()));

	return DBAttribute(child_node);
}

DBAttribute ClientDb::GetDBAttribute(const std::string& table_name, int no, const std::string& attribute_name)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[@no='%d']", table_name.c_str(), no);
	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	////A2S_ASSERT_MSG_VA(!node.empty(), ("db record find failed, table(%s), no(%d)", table_name.c_str(), no));

	xml_node child_node = node.child(attribute_name.c_str());
	////A2S_ASSERT_MSG_VA(!child_node.empty(), ("db attrib find failed, table(%s), no(%d), attrib(%s)", table_name.c_str(), no, attribute_name.c_str()));

	return DBAttribute(child_node);
}

DBAttribute ClientDb::GetDBAttribute(const std::string& table_name, const std::string& finder_attr, const std::string& finder_attr_value, const std::string& result_attr)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[%s=%s]", table_name.c_str(), finder_attr.c_str(), finder_attr_value.c_str());	

	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	////A2S_ASSERT_MSG_VA(!node.empty(), ("db record find failed, table(%s), attribute(%s), value(%s)", table_name.c_str(), finder_attr.c_str(), finder_attr_value.c_str()));

	xml_node child_node = node.child(result_attr.c_str());
	////A2S_ASSERT_MSG_VA(!child_node.empty(), ("db attrib find failed, table(%s), attribute(%s), value(%s), result_attr(%s)", table_name.c_str(), finder_attr.c_str(), finder_attr_value.c_str(), result_attr.c_str()));

	return DBAttribute(child_node);
}

DBAttribute ClientDb::GetDBAttribute(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, const std::string& finder_attr2_value, const std::string& result_attr)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[%s=%s][%s=%s]", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value.c_str());	

	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	////A2S_ASSERT_MSG_VA(!node.empty(), ("db record find failed, table(%s), attribute1(%s), value1(%s), attribute2(%s), value2(%s)", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value.c_str()));

	xml_node child_node = node.child(result_attr.c_str());
	////A2S_ASSERT_MSG_VA(!child_node.empty(), ("db attrib find failed, table(%s), attribute1(%s), value1(%s), attribute2(%s), value2(%s), result_attr(%s)", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value.c_str(), result_attr.c_str()));

	return DBAttribute(child_node);
}

DBAttribute ClientDb::GetDBAttribute(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, int finder_attr2_value, const std::string& result_attr)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[%s='%s'][%s=%d]", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value);

	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	////A2S_ASSERT_MSG_VA(!node.empty(), ("db record find failed, table(%s), attribute1(%s), value1(%s), attribute2(%s), value2(%d)", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value));

	xml_node child_node = node.child(result_attr.c_str());
	////A2S_ASSERT_MSG_VA(!child_node.empty(), ("db attrib find failed, table(%s), attribute1(%s), value1(%s), attribute2(%s), value2(%d), result_attr(%s)", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value, result_attr.c_str()));

	return DBAttribute(child_node);
}

DBAttribute ClientDb::GetDBAttribute(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, int finder_attr2_value, const std::string& finder_attr3, int finder_attr3_value, const std::string& result_attr)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[%s='%s'][%s=%d][%s=%d]", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value, finder_attr3.c_str(), finder_attr3_value);

	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	////A2S_ASSERT_MSG_VA(!node.empty(), ("db record find failed, table(%s), attribute1(%s), value1(%s), attribute2(%s), value2(%d), attribute3(%s), value3(%d)", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value, finder_attr3.c_str(), finder_attr3_value));

	xml_node child_node = node.child(result_attr.c_str());
	////A2S_ASSERT_MSG_VA(!child_node.empty(), ("db attrib find failed, table(%s), attribute1(%s), value1(%s), attribute2(%s), value2(%d), result_attr(%s), attribute3(%s), value3(%d)", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value, finder_attr3.c_str(), finder_attr3_value, result_attr.c_str()));

	return DBAttribute(child_node);
}

DBAttribute ClientDb::GetDBAttribute(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, const std::string& finder_attr2_value, const std::string& finder_attr3, const std::string& finder_attr3_value, const std::string& result_attr)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[%s=%s][%s=%s][%s=%s]", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value.c_str(), finder_attr3.c_str(), finder_attr3_value.c_str());

	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	////A2S_ASSERT_MSG_VA(!node.empty(), ("db record find failed, table(%s), attribute1(%s), value1(%s), attribute2(%s), value2(%s), attribute3(%s), value3(%s)", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value.c_str(), finder_attr3.c_str(), finder_attr3_value.c_str()));

	xml_node child_node = node.child(result_attr.c_str());
	////A2S_ASSERT_MSG_VA(!child_node.empty(), ("db attrib find failed, table(%s), attribute1(%s), value1(%s), attribute2(%s), value2(%s), attribute3(%s), value3(%s), result_attr(%s)", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value.c_str(), finder_attr3.c_str(), finder_attr3_value.c_str(), result_attr.c_str()));

	return DBAttribute(child_node);
}


void ClientDb::Init()
{
	xml_document db_list_doc;
	Helper::Xml::LoadDoc("GameRes/Db/db_list.xml", db_list_doc);

	char full_path[256];

	xml_node table_name_node = db_list_doc.child("root").child("table_name");
	for (; table_name_node; table_name_node = table_name_node.next_sibling())
	{
		std::string file_name = table_name_node.text().as_string();

		sprintf(full_path, "GameRes/Db/%s.xml", file_name.c_str());
		xml_document* xml_doc = new xml_document;
		Helper::Xml::LoadDoc(full_path, *xml_doc);

		table_map_.insert(std::make_pair(file_name, xml_doc));
	}
}

void ClientDb::Uninit()
{
	std::map<std::string, pugi::xml_document*>::iterator it = table_map_.begin();
	for (; it != table_map_.end(); ++it)
	{
		xml_document* xml_doc = it->second;
		delete xml_doc;
	}

	table_map_.clear();
}

std::string ClientDb::ToId(const std::string& table_name, int no)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[@no='%d']", table_name.c_str(), no);
	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	//A2S_ASSERT(!node.empty());

	std::string id = node.attribute("id").as_string();
	return id;
}

int ClientDb::ToNo(const std::string& table_name, const std::string& id)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[@id='%s']", table_name.c_str(), id.c_str());
	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	CC_ASSERT(!node.empty());

	int no = node.attribute("no").as_int();
	return no;
}

int ClientDb::GetNo(const std::string& table_name, const std::string& finder_attr1, const std::string& finder_attr1_value, const std::string& finder_attr2, int finder_attr2_value)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[%s='%s'][%s=%d]", table_name.c_str(), finder_attr1.c_str(), finder_attr1_value.c_str(), finder_attr2.c_str(), finder_attr2_value);

	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	CC_ASSERT(!node.empty());

	int no = node.attribute("no").as_int();
	return no;
}

bool ClientDb::VerifyNo(const std::string& table_name, int no)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[@no='%d']", table_name.c_str(), no);
	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	return !node.empty();
}

bool ClientDb::VerifyAttribute(const std::string& table_name, const std::string& attribute_name, std::string attribute_value)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[%s='%s']", table_name.c_str(), attribute_name.c_str(), attribute_value.c_str());
	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();
	return !node.empty();
}

bool ClientDb::VerifyAttribute(const std::string& table_name, const std::string& attribute_name, int attribute_value)
{
	std::map<std::string, xml_document*>::iterator it = Instance().table_map_.find(table_name);
	CC_ASSERT(it != Instance().table_map_.end());

	const xml_document* xml_doc = it->second;

	char buf[256];
	sprintf(buf, "/root/%s[%s=%d]", table_name.c_str(), attribute_name.c_str(), attribute_value);
	xpath_node query_result = xml_doc->select_node(buf);
	xml_node node = query_result.node();

	return !node.empty();
}