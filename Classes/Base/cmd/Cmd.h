#ifndef __CMD_H__
#define __CMD_H__
#pragma once
#include "CmdBase.h"

namespace cmd
{
	template <typename TYPE>
	class Cmd : 
		public CmdBase
	{
		template <typename TYPE> friend class CmdSubject;
	public:
		//S: Base
		Cmd();
		Cmd(const Cmd<TYPE>& cmd);
		Cmd(TYPE cmd_type);
		~Cmd();
		//E: Base

		TYPE cmd_type_;
	};

	template <typename TYPE>
	cmd::Cmd<TYPE>::Cmd() :
		CmdBase()
	{

	}

	template <typename TYPE>
	cmd::Cmd<TYPE>::Cmd(TYPE cmd_type)
		: CmdBase()
		, cmd_type_(cmd_type)
	{
		
	}

	template <typename TYPE>
	cmd::Cmd<TYPE>::Cmd(const Cmd<TYPE>& cmd)
		: CmdBase(cmd)
		, cmd_type_(cmd.cmd_type_)
	{

	}

	template <typename TYPE>
	cmd::Cmd<TYPE>::~Cmd()
	{
	}

}
#endif /* __CMD_H__ */