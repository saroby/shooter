#include "ShooterPch.h"
#include "Datastream.h"
#include "cocos2d.h"

DataStream::DataStream(int initialSize)
{
    m_buffer = new UInt8[initialSize];
    m_maxUsableBufferSize = initialSize;
    m_pCurrentStreamPos = m_buffer;
    m_usedBytes = 0;

    m_spStreamInfo = new StreamInfo();
    m_readOnly = false;
    m_ownBuffer = true;
}

DataStream::DataStream(UInt8* pBuffer, int size)
{
	m_buffer = pBuffer;
    m_maxUsableBufferSize = size;
    m_pCurrentStreamPos = m_buffer;
    m_usedBytes = size;

    m_spStreamInfo = new StreamInfo();
    m_readOnly = true;
    m_ownBuffer = false;
}

DataStream::DataStream(const DataStream& cpy, bool readOnly)
{
    // drw. don't optimize by sharing buffer references.
	m_buffer = new UInt8[cpy.m_maxUsableBufferSize];

    memcpy(m_buffer, cpy.m_buffer, cpy.m_usedBytes);
    m_maxUsableBufferSize = cpy.m_maxUsableBufferSize;
    m_pCurrentStreamPos = m_buffer + (cpy.m_pCurrentStreamPos - cpy.m_buffer);
    m_usedBytes = cpy.m_usedBytes;
    
	m_spStreamInfo = new StreamInfo();
	*(m_spStreamInfo) = *(cpy.m_spStreamInfo);
    m_readOnly = readOnly;  // set if this copy is read only (default is read/write)
    m_ownBuffer = true;
}

DataStream::~DataStream()
{
    if (m_ownBuffer)
    {
        delete[] m_buffer;
		m_buffer = NULL;
	}

	if (m_spStreamInfo)
	{
		delete m_spStreamInfo;
		m_spStreamInfo = NULL;
	}
}

void DataStream::Grow(int minSizeRemaining)
{
    int offset = static_cast<int>(m_pCurrentStreamPos - m_buffer);
    int growSize = MAX((int)(m_usedBytes + minSizeRemaining),
        m_maxUsableBufferSize + kDATA_STREAM_GROW_SIZE);

	UInt8* buffer = static_cast<UInt8*>(realloc(m_buffer, growSize));
	if (buffer == NULL)
		return;

    m_buffer = buffer;

    // For now, we are not worrying about running out of memory.
    m_pCurrentStreamPos = m_buffer + offset;
    m_maxUsableBufferSize = growSize;
}

void DataStream::ReadRawBuffer(void *pToBuffer, size_t dataSize) const
{
    if (!dataSize)
        return;
    CCASSERT(pToBuffer, "");
    if ((m_pCurrentStreamPos - m_buffer) + (int)dataSize > m_usedBytes)
    {
		CCASSERT(false, ("Error, request to read buffer past the end of the written data"));

        return;
    }

     memcpy(pToBuffer, m_pCurrentStreamPos, dataSize);
     m_pCurrentStreamPos += dataSize;
}

void DataStream::WriteRawBuffer(const void *pFromBuffer, size_t dataSize)
{
    if (m_readOnly)
    {
		CCASSERT(false, ("Error, write request to a read-only stream"));
        return;     // if this stream is a read-only clone
    }

    int freeBytes = m_maxUsableBufferSize - m_usedBytes;
    if (freeBytes < (int)dataSize)
    {
        Grow(dataSize);
    }

     memcpy(m_pCurrentStreamPos, pFromBuffer, dataSize);
     m_pCurrentStreamPos += dataSize;
     int upToHere = static_cast<int>(m_pCurrentStreamPos - m_buffer);

     // extend the used section if needed. This allows for "weird" seek back and overwrite.
     if (m_usedBytes < upToHere) m_usedBytes = upToHere;
}


//--------------------------------------------------------------------------------------------------
void* DataStream::GetRawBufferForWriting(size_t dataSize)
{
    if (m_readOnly)
    {
		CCASSERT(false, ("Error, write request to a read-only stream"));
        return NULL;     // if this stream is a read-only clone
    }

    int freeBytes = m_maxUsableBufferSize - m_usedBytes;
    if (freeBytes < (int)dataSize)
    {
        Grow(dataSize);
    }

    void* pBuffer = m_pCurrentStreamPos;

    m_pCurrentStreamPos += dataSize;
    int upToHere = static_cast<int>(m_pCurrentStreamPos - m_buffer);

    // extend the used section if needed. This allows for "weird" seek back and overwrite.
    if (m_usedBytes < upToHere)
    {
        m_usedBytes = upToHere;
    }
    return pBuffer;
}

void DataStream::SetRawBufferSize(size_t dataActuallyWritten)
{
    m_usedBytes = dataActuallyWritten;
    // set the current position to the end of the buffer written
    Seek(m_usedBytes);
}

void* DataStream::GetRawBufferForReading()
{
    return m_buffer;
}

// Copy a datastream into me.
void DataStream::Write(const DataStream& dsSource)
{
    if (m_readOnly)
    {
		CCASSERT(false, ("Error, write request to a read-only stream"));
        return;     // if this stream is a read-only clone
    }

    // Take the buffer data of the new stream (&val) and concatenate it to our
    // current buffer data.

    //  how big the new stream is (so we can read it back in later)
    // size_t is 64bit on 64bit platforms, we do not support strings that have
    // sizes larger than 32bits
    int size = static_cast<int>(dsSource.GetSize());

    // Note: we first stream out into the current stream's buffer.
    Write(size);

    // Take the entire contents.
    WriteRawBuffer(dsSource.m_buffer, dsSource.GetSize());

    // For legacy reasons, and if we ever go back to taking lazy copies, or sharing buffer
    // segments.  The source is now read-only.  It would cause all sorts of problems if the
    // original owner continues to write to that stream, so we disallow it.
    dsSource.SetReadOnly();
}

//--------------------------------------------------------------------------------------------------
void DataStream::Write(const DataStream &dsSource, size_t offset, size_t size)
{
    if (m_readOnly)
    {
        CCASSERT(false, ("Error, write request to a read-only stream"));
        return;     // if this stream is a read-only clone
    }

    // Take the buffer data of the new stream (&val) and concatenate it to our current buffer
    // data.

    // Take the entire contents.
    WriteRawBuffer(dsSource.m_buffer + offset, size);

    // For legacy reasons, and if we ever go back to taking lazy copies, or sharing buffer
    // segments.  The source is now read-only.  It would cause all sorts of problems if the
    // original owner continues to write to that stream, so we disallow it.
    dsSource.SetReadOnly();
}

//--------------------------------------------------------------------------------------------------
// Read a data stream from me
void DataStream::Read(DataStream& dest) const
{
    int extractSize;
    Read(extractSize);     // get size of stream we contain

    // make sure we are not reading more data than is left in the stream
    if ((m_pCurrentStreamPos - m_buffer) + extractSize > m_usedBytes)
    {
        CCASSERT(false, ("Error, request to read buffer past the end of the written data"));
        return;
    }

    UInt8 *tmp = new UInt8[extractSize];

    ReadRawBuffer(tmp, extractSize);

    // Fill up the destination
    dest.WriteRawBuffer(tmp, extractSize);
    dest.Reset();

    delete[] tmp;
}

void DataStream::Skip(const DataStream&) const
{
    int extractSize;
    Read(extractSize);     // get size of stream we contain
    SeekFromCurrentPos(extractSize);
}

bool DataStream::Seek(size_t offsetPosition) const
{
    if ((int)offsetPosition > m_usedBytes)
    {
        CCASSERT(false, ("Error, seek beyond end of DataStream"));
        return false;
    }

    m_pCurrentStreamPos = m_buffer + offsetPosition;

    return true;
}

bool DataStream::ReadAt(size_t offsetPosition, void *pToBuffer, size_t dataSize)
{
    if ((int)(offsetPosition + dataSize) > m_maxUsableBufferSize) return false;

    Seek(offsetPosition);
    ReadRawBuffer(pToBuffer, dataSize);
    return true;
}

bool DataStream::WriteAt(size_t offsetPosition, const void *pFromBuffer, size_t dataSize)
{
	if (m_readOnly || (int)(offsetPosition + dataSize) > m_maxUsableBufferSize) return false;

    Seek(offsetPosition);
    WriteRawBuffer(pFromBuffer,dataSize);
    return true;
}

// stream support methods
StreamInfo::StreamInfo()
: m_variant(0)
, m_version(0)
{
    m_endian = Endian_NetworkOrder;
}

StreamInfo::~StreamInfo()
{
}