#pragma once

class StreamInfo
{
public:
    enum Endian
    {
        /// Big Endian (non-intel architecture, typically).
        Endian_Big = 0,

        /// Little Endian (Intel machines).
        Endian_Little,

        /// Network byte order is the same as Little Endian.
        Endian_NetworkOrder = Endian_Little,
    };

    StreamInfo();
	~StreamInfo();

    inline void SetEndian(Endian eType);
    inline Endian GetEndian() const;

    /**
        Set the variant (state ID) for this stream.

        Variants are a state enumeration that can be used to differentiate a stream condition (in
        packing/unpacking).

        @param var Variant setting for the stream.
    */
    inline void SetVariant(int var);

    /**
        Get the variant (state ID) for this stream.

        Variants are a state enumeration that can be used to differentiate a stream condition (in
        packing/unpacking).

        @return Variant setting for the stream.
    */
    inline int GetVariant() const;

    /**
        Set the version ID for this stream.
        @param version Version ID for the stream.
    */
    inline void SetVersion(int version);

    /**
        Get the version ID for this stream.
        @return Version ID for the stream.
    */
    inline int GetVersion() const;

    /// Determine if the host is little Endian.
    static inline bool IsHostLittleEndian();

    /// Determine if Endian swapping is needed.
    inline bool NeedsSwap() const;

private:
    /// Variant for use with stream.
    int m_variant;

    /// Endianness of this stream.
    Endian m_endian;

    /// Version number of this stream.
    int m_version;
};