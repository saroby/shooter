#ifndef _DATA_STREAM_H__
#define _DATA_STREAM_H__

#include "IDataStream.h"
#include <string>
#include "CCStdC.h"

typedef int8_t SInt8;
typedef uint8_t UInt8;

typedef int16_t SInt16;
typedef uint16_t UInt16;

typedef int32_t SInt32;
typedef uint32_t UInt32;

typedef int64_t SInt64;
typedef uint64_t UInt64;

class DataStream
{
public:

	enum
	{
		kDATA_STREAM_INITIAL_SIZE = 128,	// default initial size
		kDATA_STREAM_GROW_SIZE = 128		// default size to grow by
	};

    /// Create with initial size
    /// @param initialSize the initial size to allocate for the DataStream
    DataStream(int initialSize = kDATA_STREAM_INITIAL_SIZE);

    /// Create a readonly DataStream from a buffer and size
    /// @param pBuffer pointer to the buffer
    /// @param size the size of the buffer
    DataStream(UInt8* pBuffer, int size);

    /// Create a copy (possibly readonly) of a DataStream from another.
    /// @param cpy DataStream to copy.
    /// @param readOnly True opens the stream in readonly state.
    DataStream(const DataStream &cpy, bool readOnly = false);

    virtual ~DataStream();

	/// @name Writers
    inline void Write(bool val);
	inline void Write(SInt8& val);
	inline void Write(UInt8& val);
	inline void Write(SInt16 val);
	inline void Write(UInt16 val);
	inline void Write(SInt32 val);
	inline void Write(UInt32 val);
	inline void Write(SInt64 val);
	inline void Write(UInt64 val);
	inline void Write(double val);
	inline void Write(const std::string& val);
	inline void Write(const wchar_t& val);

    /**
        Include an DataStream value to stream (stream into this stream).

        As a side effect, the source stream will have SetReadOnly() called on it.  This Write
        method will stream the entire stream, regardless of considerations such as the current
        offset.  The size of the buffer constitutes the first data streamed, followed by the raw
        data.

        @param i_source DataStream to be included in this stream.
    */
    void Write(const DataStream &i_source);

    /**
        A version of data-stream writing that allows sub-sections of the buffer to be written.

        This method is the only way to write less than all of the data stream.

        @param val DataStream to be included in this stream.
        @param offset Offset into val to start pulling data.
        @param size Size, in bytes, of the block to copy.
    */
    void Write(const DataStream &val, size_t offset, size_t size);
	/// @name Readers
    inline void Read(bool &val) const;
    inline void Read(double &val) const;
	inline void Read(SInt8&val) const;
	inline void Read(UInt8 &val) const;
	inline void Read(SInt16 &val) const;
	inline void Read(UInt16 &val) const;
	inline void Read(SInt32 &val) const;
	inline void Read(UInt32 &val) const;
	inline void Read(SInt64 &val) const;
	inline void Read(UInt64 &val) const;
	inline void Read(std::string& val) const;

    void Read(DataStream &val) const;
    /// @name Skip Functions
    /// The same as Read, except that it skips over the data, rather than updating the variable.
    inline void Skip(const bool& val) const;
    inline void Skip(const int& val) const;
    inline void Skip(const double& val) const;
	inline void Skip(const std::string& val) const;
	inline void Skip(const char* val) const;
	void Skip(const DataStream& val) const;

    /**
        Resets the cursor in the buffer.

        This method should be called when the local message service receives the message, and
        before the first read happens.
    */
    inline void Reset() const;

    /**
        Get the total number of bytes this stream uses.

        This method does not count padded space that is not yet allocated.

        @return Number of bytes used by this stream.
    */
    inline size_t GetSize() const;

    /**
        Read an arbitrary number of bytes starting at the current cursor position.

        @note This method is called by all the other read methods for the basic (mundane) data
        types.

        @param pToBuffer Pointer to which to write data.
        @param dataSize Number of bytes to read from stream.
    */
    void ReadRawBuffer(void *pToBuffer, size_t dataSize) const;

    /**
        Write an arbitrary number of bytes starting at the current cursor position.

        @note This method is called by all the other write methods for the basic (mundane) data
        types.

        @param pFromBuffer Pointer to address from which the raw data is read.
        @param dataSize Number of bytes to write.
    */
    void WriteRawBuffer(const void *pFromBuffer, size_t dataSize);

    /**
        Get a pointer to a buffer for writing.  Advances the stream to point to the end of the
        returned buffer.  Data written into this buffer must already by formatted in the correct
        endianess.  This is intended for special cases such as reading a data stream from the
        wire or other persisted binary form.  Call SetRawBufferSize with the actual amount of data
        written if you write less data than the amount specified by dataSize.

        @param dataSize size of the buffer to return
    */
    void* GetRawBufferForWriting(size_t dataSize);

    /**
        Get a pointer to a buffer for reading.  This is intended for special cases such as writing
        a DataStream to the wire or other persisted binary form.

        @return : a pointer to the beginning of the buffer to send over the wire/write to disk
    */
    void* GetRawBufferForReading();

    /**
        Tell DataStream how many bytes you really wrote into the buffer after calling
        GetRawBufferForWriting

        @return Number of bytes used by this stream.
    */
    void SetRawBufferSize(size_t dataActuallyWritten);

    /**
        Returns the position in the stream.

        @return Location in the stream.
    */
    inline size_t GetPos() const;

    /**
        Seek to an arbitrary absolute position in the stream.

        @param offsetPosition Location to position.
        @return True if no error; false if there is an attempt to seek beyond the end of buffer.
    */
    bool Seek(size_t offsetPosition) const;

    /**
        Seek to an arbitrary relative position in the stream.

        @param bytes Offset from current position to seek.
        @return True if no error; false if there is an attempt to seek beyond the end of buffer.
    */
    inline bool SeekFromCurrentPos(size_t bytes) const;

    /**
        Read an arbitrary number of bytes starting at the specified location within the stream.

        @param offsetPosition Where in the stream from which to read the data.
        @param pToBuffer Pointer to location where data from the stream will be written.
        @param dataSize Number of bytes to read from stream.
        @return True if seek/read is successful; false if an attempt was made to seek/read beyond
            the current stream.
    */
    bool ReadAt(size_t offsetPosition, void *pToBuffer, size_t dataSize);

    /**
        Write an arbitrary number of bytes starting at the specified location within the stream.

        @param offsetPosition Where in the stream to write the data.
        @param pFromBuffer Pointer to where raw data will be read.
        @param dataSize Number of bytes to write to the stream.
        @return True if seek/write successful; false otherwise.
    */
    bool WriteAt(size_t offsetPosition, const void *pFromBuffer, size_t dataSize);

    /// @name Stream Support Methods
    //@{
    /**
        Set the endianness for the associated stream.

        @param eType Endian setting for the stream.
    */
    inline void SetEndian(StreamInfo::Endian eType);

    /**
        Get the endianness for the associated stream.

        @return Endian setting for the stream.
    */
    inline StreamInfo::Endian GetEndian() const;

    /**
        Set the variant (state ID) for this stream.

        Variants are a state enumeration that can be used to differentiate a stream condition (in
        packing/unpacking).

        @param var Variant setting for the stream.
    */
    inline void SetVariant(int var);

    /**
        Get the variant (state ID) for this stream.

        Variants are a state enumeration that can be used to differentiate a stream condition (in
        packing/unpacking).

        @return Variant setting for the stream.
    */
    inline int GetVariant() const;

    /**
        Set the version ID for this stream.

        @param version Version ID for this stream.
    */
    inline void SetVersion(int version);

    /**
        Get the version ID for this stream.

        @return Version ID for this stream.
    */
    inline int GetVersion() const;

    /**
        After writing to a stream has completed, it may be set as read-only to prevent any further
        writes.

        Once a stream becomes read-only, it can never become writable again.  This function is
        const because read-only status is a mutable property.
    */
    inline void SetReadOnly() const;

    /**
        Check if the stream is read-only.

        @return True if stream is read-only; false otherwise.
    */
    inline bool IsReadOnly() const;
    //@}

	template<typename T>
	void Write(T val)
	{
		if (sizeof(T) > 4)
		{
			Swap64(reinterpret_cast< UInt8* >(&val));
		}
		else
		{
			Swap32(reinterpret_cast< UInt8* >(&val));
		}

		WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(T));
	}

	template<typename T>
	void Read(T& val) const
	{
		ReadRawBuffer(&val, sizeof(T));
		if (sizeof(T) > 4)
		{
			Swap64(reinterpret_cast< UInt8* >(&val));
		}
		else
		{
			Swap32(reinterpret_cast< UInt8* >(&val));
		}
	}

public:
    /**
        Return a raw pointer to the current location in the stream.

        @note This method should not be used directly.  It is implementation-dependent and is only
            needed to make the process of sending over the network use fewer copies.
    */
    inline const UInt8* GetRawBuffer();

    /// Get the number of bytes remaining.
    inline size_t GetRawBytesRemaining();

protected:
    /// Grows the size of the simple buffer.
    void Grow(int minSizeRemaining = 0);

    /// Simple buffer for data stream.
    UInt8* m_buffer;

    // Maximum usable buffer size; can grow as buffer is reallocated.
    int m_maxUsableBufferSize;

    // Number of bytes written to this stream.
    int m_usedBytes;

    // Address to read or write the next byte.
    mutable UInt8 *m_pCurrentStreamPos;

    //@{
    /// Byte swap helpers.
    inline void Swap16(UInt8* buf) const;
    inline void Swap32(UInt8* buf) const;
    inline void Swap64(UInt8* buf) const;
    //@}

    /// Smart pointer to support information for the stream.
    StreamInfo* m_spStreamInfo;

    /// Read-only flag (for cloned streams used in behaviors and other instances).
    mutable bool m_readOnly;

    /// Flag to tell us if this DataStream instance owns the buffer it contains
    bool m_ownBuffer;
};

/// @name Template definitions for stream operators from DataStream.h
template<typename T>
DataStream& operator<< (DataStream& str, const T &val)
{
    str.Write(val);
    return str;
}

template<typename T>
const DataStream& operator>> (const DataStream& str, T &val)
{
    str.Read(val);
    return str;
}

template<typename T>
void DataStreamSkip(const DataStream& str, const T &val)
{
    str.Skip(val);
}

#include "DataStream.inl"

#endif  // _DATA_STREAM_H__