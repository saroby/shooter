inline void DataStream::Write(bool val)
{
    // Unlike most of the other write methods to stream, we must cast bool to one of the known
    // types since standard "bool" can be different sizes across platforms.
    UInt8 bval = val;
    WriteRawBuffer(reinterpret_cast < void * > (&bval), sizeof(UInt8));
}

void DataStream::Write(SInt16 val)
{
	Swap16(reinterpret_cast< UInt8* >(&val));
	WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(SInt16));
}

void DataStream::Write(UInt16 val)
{
	Swap16(reinterpret_cast< UInt8* >(&val));
	WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(UInt16));
}

void DataStream::Write(SInt32 val)
{
	Swap16(reinterpret_cast< UInt8* >(&val));
	WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(SInt32));
}

void DataStream::Write(UInt32 val)
{
	Swap16(reinterpret_cast< UInt8* >(&val));
	WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(UInt32));
}

void DataStream::Write(SInt64 val)
{
	Swap16(reinterpret_cast< UInt8* >(&val));
	WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(SInt64));
}

void DataStream::Write(UInt64 val)
{
	Swap16(reinterpret_cast< UInt8* >(&val));
	WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(UInt64));
}

inline void DataStream::Write(double val)
{
    Swap64(reinterpret_cast< UInt8* >(&val));
    WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(double));
}

inline void DataStream::Write(const std::string& val)
{
    // size_t is 64bit on 64bit platforms.  Strings that have sizes larger than 32bits are not
    // supported.
    int size = static_cast<int>(val.size());
    Write(size);
    WriteRawBuffer(val.c_str(), size);   // val.data() pts to raw buffer in efd::utf8string.
}

inline void DataStream::Write(SInt8& val)
{
	WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(SInt8));
}

void DataStream::Write(UInt8& val)
{
	WriteRawBuffer(reinterpret_cast < void * > (&val), sizeof(UInt8));
}

inline size_t DataStream::GetPos() const
{
    return m_pCurrentStreamPos - m_buffer;
}

inline void DataStream::Read(bool& val) const
{
    UInt8    bval;
    // Unlike most of the other read methods to stream, use the known sized value from stream, and
    // then give it to the user, since standard "bool" can be different sizes across platforms.
    ReadRawBuffer(&bval, sizeof(bval));
    val = bval ? true : false;
}

inline void DataStream::Read(SInt8& val) const
{
	ReadRawBuffer(&val, sizeof(UInt8));
}

inline void DataStream::Read(UInt8& val) const
{
	ReadRawBuffer(&val, sizeof(UInt8));
}

inline void DataStream::Read(SInt16& val) const
{
	ReadRawBuffer(&val, sizeof(SInt16));
	Swap16(reinterpret_cast< UInt8* >(&val));
}

inline void DataStream::Read(UInt16& val) const
{
	ReadRawBuffer(&val, sizeof(UInt16));
	Swap16(reinterpret_cast< UInt8* >(&val));
}

inline void DataStream::Read(SInt32& val) const
{
	ReadRawBuffer(&val, sizeof(SInt32));
	Swap16(reinterpret_cast< UInt8* >(&val));
}

inline void DataStream::Read(UInt32& val) const
{
	ReadRawBuffer(&val, sizeof(UInt32));
	Swap16(reinterpret_cast< UInt8* >(&val));
}

inline void DataStream::Read(SInt64& val) const
{
	ReadRawBuffer(&val, sizeof(SInt64));
	Swap16(reinterpret_cast< UInt8* >(&val));
}

inline void DataStream::Read(UInt64& val) const
{
	ReadRawBuffer(&val, sizeof(UInt64));
	Swap16(reinterpret_cast< UInt8* >(&val));
}

inline void DataStream::Read(double& val) const
{
    ReadRawBuffer(&val, sizeof(double));
    Swap64(reinterpret_cast< UInt8* >(&val));
}

inline void DataStream::Read(std::string& val) const
{
    int size = 0;
    Read(size);

    // DT20092 Security fix: before blindly allocating a potentially huge amount of memory here, we
    // should ensure that the DataStream has at least size remaining bytes in the stream.  Also, it
    // is critical that we ensure the Read above actually succeeded before we continue or we will
    // use the wrong size to allocate!

    // This read into a temporary buffer is required since the string may actually cross multiple
    // internal buffers.  Also, the NULL terminator was stripped off when this stream was written,
    // so it should be replaced.
    char* cTmp = new char[size+1];
    ReadRawBuffer(cTmp, size);
    cTmp[size] = 0;

    val = cTmp;

	delete[] cTmp;
}

inline void DataStream::Skip(const bool&) const
{
    // Unlike most of the other read methods to stream, use the known sized value from stream;
    // then give it to the user, since standard "bool" can be different sizes across platforms.
    SeekFromCurrentPos(sizeof(UInt8));
}

inline void DataStream::Skip(const int&) const
{
    SeekFromCurrentPos(sizeof(int));
}

inline void DataStream::Skip(const double&) const
{
    SeekFromCurrentPos(sizeof(double));
}

inline void DataStream::Skip(const std::string&) const
{
    int size = 0;
    Read(size);

    // DT20092 Security fix: before blindly allocating a potentially huge amount of memory here, we
    // should ensure that the DataStream has at least size remaining bytes in the stream.  Also, it
    // is critical that we ensure the Read above actually succeeded before we continue or we will
    // use the wrong size to allocate!

    SeekFromCurrentPos(size);
}

inline void DataStream::Skip(const char*) const
{
	int size = 0;
	Read(size);

	SeekFromCurrentPos(size);
}

inline void DataStream::SetEndian(StreamInfo::Endian eType)
{
    m_spStreamInfo->SetEndian(eType);
}

inline StreamInfo::Endian DataStream::GetEndian () const
{
    return m_spStreamInfo->GetEndian();
}

//------------------------------------------------------------------------------------------------
// Set variant (state ID) for this stream.  Variants are a state enumeration that can be used to
// differentiate a stream condition (in packing/unpacking).
inline void DataStream::SetVariant(int var)
{
    m_spStreamInfo->SetVariant(var);
}

//------------------------------------------------------------------------------------------------
// Get variant (state ID) for this stream.  Variants are a state enumeration that can be used to
// differentiate a stream condition (in packing/unpacking).
inline int DataStream::GetVariant() const
{
    return m_spStreamInfo->GetVariant();
}

//------------------------------------------------------------------------------------------------
// Set version ID for this stream.
inline void DataStream::SetVersion(int version)
{
    m_spStreamInfo->SetVersion(version);
}

//------------------------------------------------------------------------------------------------
// Get version ID for this stream.
inline int DataStream::GetVersion() const
{
    return m_spStreamInfo->GetVersion();
}

inline void DataStream::SetReadOnly() const
{
    m_readOnly = true;
}

inline bool DataStream::IsReadOnly() const
{
    return m_readOnly;
}

inline void DataStream::Reset() const
{
    m_pCurrentStreamPos = m_buffer;
}

inline size_t DataStream::GetSize() const
{
    return m_usedBytes;
}

bool DataStream::SeekFromCurrentPos(size_t bytes) const
{
    return Seek(GetPos() + bytes);
}

//------------------------------------------------------------------------------------------------
// Byte swap helpers.
//------------------------------------------------------------------------------------------------
void DataStream::Swap16 (UInt8* buf) const
{
    if (m_spStreamInfo->NeedsSwap())
    {
        UInt8 tmp = buf[0];
        buf[0] = buf[1];
        buf[1] = tmp;
    }
}

inline void DataStream::Swap32 (UInt8* buf) const
{
    if (m_spStreamInfo->NeedsSwap())
    {
        UInt8 tmp = buf[0];
        buf[0] = buf[3];
        buf[3] = tmp;
        tmp = buf[1];
        buf[1] = buf[2];
        buf[2] = tmp;
    }
}

inline void DataStream::Swap64 (UInt8* buf) const
{
    if (m_spStreamInfo->NeedsSwap())
    {
        UInt8 tmp = buf[0];
        buf[0] = buf[7];
        buf[7] = tmp;
        tmp = buf[1];
        buf[1] = buf[6];
        buf[6] = tmp;
        tmp = buf[2];
        buf[2] = buf[5];
        buf[5] = tmp;
        tmp = buf[3];
        buf[3] = buf[4];
        buf[4] = tmp;
    }
}

inline const UInt8* DataStream::GetRawBuffer()
{
    return m_pCurrentStreamPos;
}

inline size_t DataStream::GetRawBytesRemaining()
{
    return m_pCurrentStreamPos - m_buffer;
}

//------------------------------------------------------------------------------------------------
// StreamInfo support methods
//------------------------------------------------------------------------------------------------
// Set Endianess for this stream.
inline void StreamInfo::SetEndian(StreamInfo::Endian eType)
{
    m_endian = eType;
}

// Get Endianess for this stream.
inline StreamInfo::Endian StreamInfo::GetEndian () const
{
    return m_endian;
}

//------------------------------------------------------------------------------------------------
// Set variant (state ID) for this stream.  Variants are a state enumeration that can be used to
// differentiate a stream condition (in packing/unpacking).
inline void StreamInfo::SetVariant(int var)
{
    m_variant = var;
}

//------------------------------------------------------------------------------------------------
// Get variant (state ID) for this stream.  Variants are a state enumeration that can be used to
// differentiate a stream condition (in packing/unpacking).
inline int StreamInfo::GetVariant() const
{
    return m_variant;
}

//------------------------------------------------------------------------------------------------
// Set version ID for this stream.
inline void StreamInfo::SetVersion(int version)
{
    m_version = version;
}

//------------------------------------------------------------------------------------------------
// Get version ID for this stream.
inline int StreamInfo::GetVersion() const
{
    return m_version;
}

//------------------------------------------------------------------------------------------------
inline bool StreamInfo::IsHostLittleEndian()
{
    static union { int i; char a[4]; } u = {1};
    return u.a[0] == 1;
}

//------------------------------------------------------------------------------------------------
inline bool StreamInfo::NeedsSwap() const
{
    return (((m_endian == StreamInfo::Endian_Little) && !IsHostLittleEndian()) ||
             ((m_endian == StreamInfo::Endian_Big) && IsHostLittleEndian()));
}
