#include "ShooterPch.h"
#include "CmdBase.h"

using namespace cmd;

//////////////////////////////////////////////////////////////////////////

CmdBase::CmdBase() : 
data_stream_(nullptr)
{
	data_stream_ = new DataStream();
}

CmdBase::CmdBase(const CmdBase& command)
{
	if (command.data_stream_ != nullptr)
	{
		data_stream_ = new DataStream(*command.data_stream_);
	}
}

CmdBase::~CmdBase()
{
	if (this->data_stream_ != nullptr)
	{
		delete data_stream_;
		data_stream_ = nullptr;
	}
}
