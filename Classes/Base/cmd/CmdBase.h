#ifndef __CMD_BASE_H__
#define __CMD_BASE_H__
#pragma once
#include "DataStream/DataStream.h"

namespace cmd
{
	class CmdBase
	{
	public:
		//S: Base
		CmdBase();
		CmdBase(const CmdBase& command);
		virtual ~CmdBase();
		//E: Base

//	protected:
		//S: Characteristic
		DataStream* data_stream_;
		//E: Characteristic
	};
}
#endif /* __CMD_BASE_H__ */


