#ifndef __CMD_SUBJECT_H__
#define __CMD_SUBJECT_H__
#pragma once
#include "CmdHandler.h"
#include <map>
#include <vector>

namespace cmd
{
	template <typename _TYPE>
	class CmdSubject
	{
		template <typename _TYPE> friend class CmdHandler;
	public:
		typedef std::vector<CmdHandler<_TYPE>*> Handlers;
		//S: Base
		CmdSubject();
		virtual ~CmdSubject();
		//E: Base
		void RegisterHandlerAll(CmdHandler<_TYPE>* handler);
		void UnregisterHandlerAll(CmdHandler<_TYPE>* handler);
		void Broadcast(_TYPE cmd_type);
		void Broadcast(const Cmd<_TYPE>& cmd);
	private:
		void RegisterHandler(_TYPE command_type, CmdHandler<_TYPE>* handler);
		void UnregisterHandler(_TYPE command_type, CmdHandler<_TYPE>* handler);

		std::map<_TYPE, Handlers> handlers_map_;
	};

	template <typename _TYPE>
	cmd::CmdSubject<_TYPE>::CmdSubject()
	{

	}

	template <typename _TYPE>
	cmd::CmdSubject<_TYPE>::~CmdSubject()
	{
		for (auto handlers_map_iter = handlers_map_.begin(); handlers_map_iter != handlers_map_.end();)
		{
			Handlers& handlers = handlers_map_iter->second;

			for (auto handlers_iter = handlers.begin(); handlers_iter != handlers.end();)
			{
				handlers_iter = handlers.erase(handlers_iter);
			}

			handlers_map_iter = handlers_map_.erase(handlers_map_iter);
		}
	}

	template <typename _TYPE>
	void cmd::CmdSubject<_TYPE>::RegisterHandler(_TYPE command_type, CmdHandler<_TYPE>* handler)
	{
		auto handlers_map_iter = handlers_map_.find(command_type);
		if (handlers_map_iter == handlers_map_.end())
		{
			Handlers handlers;
			handlers.push_back(handler);
			handlers_map_.insert(std::make_pair(command_type, handlers));
		}
		else
		{
			Handlers& handlers = handlers_map_iter->second;

			auto handlers_iter = std::find(handlers.begin(), handlers.end(), handler);
			if (handlers_iter == handlers.end())
			{
				handlers.push_back(handler);
			}
		}
	}

	template <typename _TYPE>
	void cmd::CmdSubject<_TYPE>::UnregisterHandler(_TYPE command_type, CmdHandler<_TYPE>* handler)
	{
		auto handlers_map_iter = handlers_map_.find(command_type);
		if (handlers_map_iter == handlers_map_.end())
		{
			return;
		}

		auto& handlers = handlers_map_iter->second;

		for (auto handlers_iter = handlers.begin(); handlers_iter != handlers.end();)
		{
			if (*handlers_iter == handler)
			{
				handlers_iter = handlers.erase(handlers_iter);
				break;
			}
			else
			{
				++handlers_iter;
			}
		}

		if (handlers.empty())
		{
			handlers_map_.erase(handlers_map_iter);
		}
	}



	template <typename _TYPE>
	void cmd::CmdSubject<_TYPE>::RegisterHandlerAll(CmdHandler<_TYPE>* handler)
	{
		for (auto iter = handlers_map_.begin(); iter != handlers_map_.end(); ++iter)
		{
			_TYPE type = iter->first;
			RegisterHandler(type, handler);
		}
	}

	template <typename _TYPE>
	void cmd::CmdSubject<_TYPE>::UnregisterHandlerAll(CmdHandler<_TYPE>* handler)
	{
		for (auto iter = handlers_map_.begin(); iter != handlers_map_.end();)
		{
			auto& handlers = iter->second;

			for (auto handlers_iter = handlers.begin(); handlers_iter != handlers.end();)
			{
				if (*handlers_iter == handler)
				{
					handlers_iter = handlers.erase(handlers_iter);
					break;
				}
				else
				{
					++handlers_iter;
				}
			}

			if (handlers.empty())
			{
				iter = handlers_map_.erase(iter);
			}
			else
			{
				++iter;
			}
		}
	}


	template <typename _TYPE>
	void cmd::CmdSubject<_TYPE>::Broadcast(_TYPE cmd_type)
	{
		Cmd<_TYPE> command = Cmd<_TYPE>();
		command.cmd_type_ = cmd_type;

		this->Broadcast(command);
	}

	template <typename _TYPE>
	void cmd::CmdSubject<_TYPE>::Broadcast(const Cmd<_TYPE>& command)
	{
		auto handlers_iter = handlers_map_.find(command.cmd_type_);
		if (handlers_iter == handlers_map_.end()) { return; }

		Handlers handlers = handlers_iter->second;

		if (handlers.begin() == handlers.end())
		{
#ifdef WIN32
			CCASSERT(false, ("Did not carry out this ui command type"));
#endif
		}
		else
		{
			for (auto iter = handlers.begin(); iter != handlers.end(); iter++)
			{
				if (command.data_stream_)
				{
					command.data_stream_->Reset();
				}

				(*iter)->HandleCmd(command);
			}
		}
	}
}

#endif /* defined(__CMD_SUBJECT_H__) */
