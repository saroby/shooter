#ifndef __CMD_MANAGER_H__
#define __CMD_MANAGER_H__
#pragma once
#include "Base/Singleton.h"
#include "CmdSubject.h"

namespace cmd
{
	template <typename _TYPE>
	class CmdManager : 
		public CmdSubject<_TYPE>
	{
	public:
		//S: Base
		CmdManager();
		~CmdManager();
		//S: Base
	};

	template <typename _TYPE>
	cmd::CmdManager<_TYPE>::CmdManager() :
		CmdSubject<_TYPE>()
	{

	}

	template <typename _TYPE>
	CmdManager<_TYPE>::~CmdManager()
	{

	}
}

#endif /* __CMD_MANAGER_H__ */