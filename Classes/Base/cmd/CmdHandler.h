#ifndef __CMD_HANDLER_H__
#define __CMD_HANDLER_H__
#pragma once
#include "Cmd.h"
#include "CmdHandler.h"
#include "CmdSubject.h"

namespace cmd
{
	template <typename _TYPE>
	class CmdHandler
	{
		template <typename _TYPE> friend class CmdSubject;
	public:
		//S: Base
		CmdHandler();
		virtual ~CmdHandler();
		//E: Base
		virtual void HandleCmd(const Cmd<_TYPE>& cmd) = 0;
		virtual void RegisterHandleCmd(CmdSubject<_TYPE>* subject, _TYPE handle_type);
	private:
		CmdSubject<_TYPE>* subject_;
	};

	template <typename _TYPE>
	cmd::CmdHandler<_TYPE>::CmdHandler()
		: subject_(nullptr)
	{

	}

	template <typename _TYPE>
	cmd::CmdHandler<_TYPE>::~CmdHandler()
	{
		if (subject_ != nullptr)
		{
			subject_->UnregisterHandlerAll(this);
		}
	}

	template <typename _TYPE>
	void cmd::CmdHandler<_TYPE>::RegisterHandleCmd(CmdSubject<_TYPE>* subject, _TYPE handle_type)
	{
		if (subject == nullptr)
		{
			return;
		}

		subject_ = subject;
		subject->RegisterHandler(handle_type, this);
	}
}

#endif