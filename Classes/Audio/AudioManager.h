

class AudioManager
{
public:
	AudioManager();
	~AudioManager();

	static AudioManager* Get();
	static void Release();

	enum class AudioType
	{
		BGM,
		Effect,
	};

	bool Load();

	bool Play(AudioType audio_type, const std::string& audio_id);
	void Stop(AudioType audio_type, const std::string& audio_id);
	void Mute(AudioType audio_type);
	void Pause(AudioType audio_type);
	void Resume(AudioType audio_type);
private:
};