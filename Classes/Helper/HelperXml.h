#pragma once
#include "cocos2d.h"
#include "Base/pugixml/src/pugixml.hpp"

namespace Helper
{
	namespace Xml
	{
		static void LoadDoc(const char* relative_file_path, pugi::xml_document& xml_doc)
		{
			std::string full_path = cocos2d::FileUtils::getInstance()->getWritablePath() + relative_file_path;

			unsigned char* buffer = NULL;
			ssize_t buffer_size = 0;
			buffer = cocos2d::FileUtils::getInstance()->getFileData(full_path.c_str(), "r", &buffer_size);
			
			auto result = xml_doc.load_buffer(buffer, buffer_size);
			//CC_UNUSED_PARAM(result);

#ifdef WIN32
			if ((bool)result == false)
			{
				std::string error_desc = result.description();
				char buf[1024];
				sprintf(buf, "error : %s\nfile : %s", error_desc.c_str(), full_path.c_str());
				MessageBoxA(NULL, buf, "XML error", MB_OK);
			}
#endif // WIN32

			delete[] buffer;
		};

		static bool LoadFromWriteablePath(const char* relative_file_path, pugi::xml_document& xml_doc);
	}
}
