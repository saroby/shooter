#include "ShooterPch.h"
#include "Config.h"

using namespace tinyxml2;

Config::Config(void)
	: use_frame_info_(false)
	, use_test_ui_(false)
{
	Init();
}

bool Config::Init()
{
	tinyxml2::XMLDocument doc;

	//auto tmep = cocos2d::CCFileUtils::getInstance()->getDataFromFile("config.xml");
	std::string file_path = cocos2d::FileUtils::getInstance()->fullPathForFilename("Xml/config.xml");
	cocos2d::log("external file path = %s",file_path.c_str());

	if (doc.LoadFile(file_path.c_str()) != XML_NO_ERROR)
	{
		cocos2d::log("config.xml load error!");
		return false;
	}
	
	XMLElement* root = doc.RootElement();
	XMLElement* element;

	element = root->FirstChildElement("use_frame_info");
	if (element != nullptr)
	{
		element->QueryBoolText(&use_frame_info_);
	}

	element = root->FirstChildElement("use_test_ui");
	if (element != nullptr)
	{
		element->QueryBoolText(&use_test_ui_);
	}

	cocos2d::log("config.xml load success.");

	return true;
}
