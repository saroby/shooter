#include "ShooterPch.h"
#include "HelperXml.h"

bool Helper::Xml::LoadFromWriteablePath(const char* relative_file_path, pugi::xml_document& xml_doc)
{
	bool load_success = false;
	unsigned char* buffer = NULL;
	ssize_t buffer_size = 0;
	std::string writeable_path = cocos2d::FileUtils::getInstance()->getWritablePath();
	writeable_path += relative_file_path;

	buffer = cocos2d::FileUtils::getInstance()->getFileData(writeable_path.c_str(), "r", &buffer_size);
	//buffer = FileUtils::getInstance()->getFileDataOriginal(writeable_path.c_str(), "r", &buffer_size, false);
	if (!xml_doc.load_buffer(buffer, buffer_size))
	{
		//A2S_ASSERT(false);		
	}

	if (NULL != buffer)
	{
		delete[] buffer;
		load_success = true;
	}

	return load_success;
}

