#pragma once
#include "AppDefines.h"
#include "Base/Singleton.h"

class Config :
	public Singleton<Config>
{
public:
	Config(void);

	bool use_frame_info_;
	bool use_test_ui_;
private:
	bool Init();

};

