#pragma once
#include "cocostudio/CocoStudio.h"

namespace Helper
{
	class cocos2d::Node;
	template <typename _TYPE>
	class HelperNodeReader
		: public cocostudio::NodeReader
	{
		static HelperNodeReader<_TYPE>* inst_;
	public:
		HelperNodeReader();
		~HelperNodeReader();

		//static HelperNodeReader<_TYPE>* getInstance();
		//static void purge();
		//cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeOptions);
	};

	//template <typename _TYPE>
	//HelperNodeReader<_TYPE>* Helper::HelperNodeReader<_TYPE>::getInstance()
	//{
	//	if (inst_ == nullptr)
	//	{
	//		inst_ = new HelperNodeReader<_TYPE>();
	//	}

	//	return inst_;
	//}

	template <typename _TYPE>
	Helper::HelperNodeReader<_TYPE>::HelperNodeReader()
	{

	}

	template <typename _TYPE>
	Helper::HelperNodeReader<_TYPE>::~HelperNodeReader()
	{

	}

	//template <typename _TYPE>
	//void Helper::HelperNodeReader<_TYPE>::purge()
	//{
	//	CC_SAFE_DELETE(inst_);
	//}

	//template <typename _TYPE>
	//cocos2d::Node* Helper::HelperNodeReader<_TYPE>::createNodeWithFlatBuffers(const flatbuffers::Table* nodeOptions)
	//{
	//	auto node = _TYPE::create();
	//	setPropsWithFlatBuffers(node, nodeOptions);
	//	return node;
	//}
}


