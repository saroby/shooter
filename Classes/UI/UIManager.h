#ifndef __UI_MANAGER_H__
#define __UI_MANAGER_H__
#pragma once
#include "UIDefines.h"
#include "UILayerAgent.h"
#include "UISceneAgent.h"
#include "UINodeAgent.h"

using namespace UI;

namespace UI
{
	struct UIInfo
	{
		enum class Type
		{
			Invaild,
			Scene,
			Layer,
			Node
		};
		enum class ZOrder
		{
			Invaild,
			Page,
			Popup,
			Msg,
			Wait,
			Error,
		};

		static UIInfo::Type GetType(std::string value)
		{
			if (value == "scene") return Type::Scene;
			if (value == "layer") return Type::Layer;
			if (value == "node") return Type::Node;
			return Type::Invaild;
		}

		static UIInfo::ZOrder GetZOrder(std::string value)
		{
			if (value == "page") return ZOrder::Page;
			if (value == "popup") return ZOrder::Popup;
			if (value == "msg") return ZOrder::Msg;
			if (value == "wait") return ZOrder::Wait;
			if (value == "error") return ZOrder::Error;
			return ZOrder::Invaild;
		}

		std::string name_;
		ZOrder z_orer_;
		std::string file_;
		Type type_;
		
		UIInfo(tinyxml2::XMLElement* element);
	};
}

namespace UI
{
	class UIManager
		: public Singleton<UIManager>
		, public CmdManager<UINotifyType>
		, public CmdHandler<UINotifyType>
	{
	public:
		explicit UIManager();
		~UIManager();

		void HandleCmd(const Cmd<UINotifyType>& cmd);
		static cocos2d::Node* LoadNode(UIInfo* ui_info);
		static cocostudio::timeline::ActionTimeline* LoadActionTimeline(UIInfo* ui_info);

		UILayerAgent layer_agent_;
		UISceneAgent scene_agent_;
		UINodeAgent node_agent_;
	private:
		void Init();
		void Uninit();
		void RegisterCustomClass();

		bool CheckPageBackEnable();

		bool page_changable_;
	};
}

#endif