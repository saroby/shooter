#pragma once
#include "UIDefines.h"

namespace UI
{
	class UIInfo;
	class SceneBase;
	class UISceneAgent
	{
		friend class UIManager;
	public:
		UISceneAgent();
		~UISceneAgent();

		void Show(UISceneType type);
		SceneBase* GetCurrSceneBase();
	private:
		SceneBase* Create(UISceneType type);

		SceneBase* curr_scene_base_;
		std::map<UISceneType, UIInfo*> ui_info_map_;
	};
}


