#pragma once
#include "UIDefines.h"

namespace UI
{
	class UIInfo;
	class UINodeAgent
	{
		friend class UIManager;
	public:
		UINodeAgent();
		~UINodeAgent();
		void Show(UINodeType type);
	private:
		Node* Create(UINodeType type);

		std::map<UINodeType, UIInfo*> ui_info_map_;
	};

}


