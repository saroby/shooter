#pragma once
#include "UIDefines.h"

namespace UI
{
	class UIInfo;
	class LayerBase;
	class UILayerAgent
	{
		friend class UIManager;
	public:
		UILayerAgent();
		~UILayerAgent();

		void Show(UILayerType type);
		void Back();
		void Clear();
			
		LayerBase* Create(UILayerType type);
		
		bool CheckVisible(UILayerType type);
	private:
		void Init();
		void Uninit();

		LayerBase* curr_page_;
		std::stack<LayerBase*> page_stack_;
		std::map<UILayerType, UIInfo*> ui_info_map_;
	};
}