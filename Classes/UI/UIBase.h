#ifndef __UI_BASE_H__
#define __UI_BASE_H__
#pragma once
#include "UIDefines.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include "cocosbuilder/CocosBuilder.h"
/************************************************************************/
/* Cocos Studio - Custom class                                          */
/************************************************************************/
#include "cocostudio/CocosStudioExport.h"
#include "cocostudio/WidgetReader/NodeReader/NodeReader.h"

#define GLUE_LOADER(__loader_name__, __load_class__) \
class __loader_name__ \
: public cocostudio::NodeReader \
{\
	static __loader_name__* inst_;\
public:\
	__loader_name__() {};\
	~__loader_name__() {};\
	static __loader_name__* getInstance()\
	{\
		if (inst_ == nullptr)\
		{\
			inst_ = new __loader_name__();\
		}\
		return inst_;\
	}\
	static void purge()\
	{\
		CC_SAFE_DELETE(inst_);\
	};\
	cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeOptions)\
	{\
		__load_class__* node = __load_class__::create();\
		setPropsWithFlatBuffers(node, nodeOptions);\
		return node;\
	};\
};

/************************************************************************/
/* UI                                                                   */
/************************************************************************/

using namespace cocostudio::timeline;


class ActionCallbackTimeline
	: public ActionTimeline
{
public:
	ActionCallbackTimeline();
	~ActionCallbackTimeline();

private:
};

typedef std::function<void(Ref*)> AniCBFunc;

namespace UI
{
	class UIBase 
	{
	public:
		UIBase(void);
		virtual ~UIBase(void);

		virtual bool Show();
		virtual bool Hide();

		virtual void SetActionTimeline(ActionTimeline* action_timeline);
		virtual void RunAni(std::string ani_key, bool loop = false);
		virtual void RunAni(std::string ani_key, bool loop, AniCBFunc end_cb = nullptr);
	protected:
		virtual bool RegisterAniCB(const std::string& ani_key, AniCBFunc end_cb);
		virtual void UnregisterAniCB(const std::string& ani_key, AniCBFunc end_cb);
		virtual void AniEndCb(const std::string& ani_key);

		ActionTimeline* action_timeline_;
		std::map<std::string, std::vector<AniCBFunc>> ani_end_cb_map_;
	};
}

#endif