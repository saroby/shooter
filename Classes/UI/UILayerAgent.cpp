#include "ShooterPch.h"
#include "UILayerAgent.h"
#include "Layer/LayerBase.h"
#include "Scene/SceneBase.h"
#include "UIManager.h"

using namespace UI;

UI::UILayerAgent::UILayerAgent()
	: curr_page_(nullptr)
{

}

UI::UILayerAgent::~UILayerAgent()
{

}

void UI::UILayerAgent::Show( UILayerType type )
{
	auto running_scene = UIManager::Instance().scene_agent_.GetCurrSceneBase();
	auto ui_info_iter = ui_info_map_.find(type);
	if (!running_scene || 
		ui_info_iter == ui_info_map_.end())
	{
		return;
	}

	UIInfo* ui_info = ui_info_iter->second;

	Node* node = UIManager::LoadNode(ui_info);
	CCASSERT(node, "node is null!");
	auto new_layer_base = static_cast<LayerBase*>(node);

	switch (ui_info->z_orer_)
	{
	case UIInfo::ZOrder::Msg:
	{

	}
	break;
	case UIInfo::ZOrder::Page:
	{
		if (curr_page_ != nullptr &&
			curr_page_->GetUILayerType() == type)
		{
			CCASSERT(false, "current layer!");
			return;
		}

		curr_page_ = new_layer_base;
		page_stack_.push(curr_page_);
	}
	break;
	case UIInfo::ZOrder::Popup:
	{
		LayerBase* new_layer_base = Create(type);
		CCASSERT(new_layer_base, "node is null!");
		running_scene->addChild(new_layer_base, (int)ui_info->z_orer_);
	}
	break;
	case UIInfo::ZOrder::Wait:
	{

	}
	break;
	default:
		CCASSERT(false, "");
		break;
	}

	//CCLOG("factor: %f", Director::getInstance()->getContentScaleFactor());
	//auto children = new_layer_base->getChildren();
	//for (auto& child : children)
	//{
	//	auto sprite = dynamic_cast<Sprite*>(child);
	//	if (sprite)
	//	{
	//		sprite->setContentSize(sprite->getContentSize()*Director::getInstance()->getContentScaleFactor());
	//		
	//	}
	//}
	//new_layer_base->getContentSize().width * Director::getInstance()->getContentScaleFactor();
	//new_layer_base->setScale(Director::getInstance()->getContentScaleFactor());
	//new_layer_base->setContentSize(new_layer_base->getContentSize()*Director::getInstance()->getContentScaleFactor());
}

void UI::UILayerAgent::Back()
{
	if (page_stack_.size() < 1)
	{
		cocos2d::log("LobbyLayerAgent empty.");
		return;
	}

	curr_page_ = page_stack_.top();
	page_stack_.pop();
}

void UI::UILayerAgent::Clear()
{
	while (!page_stack_.empty())
	{
		page_stack_.pop();
	}
}


LayerBase* UI::UILayerAgent::Create(UILayerType type)
{
	Node* node = UIManager::LoadNode(ui_info_map_.at(type));
	CCASSERT(node, "node is null!");

	LayerBase* new_layer_base = dynamic_cast<LayerBase*>(node);

	auto action_timeline = UIManager::LoadActionTimeline(ui_info_map_.at(type));
	if (action_timeline)
	{
		new_layer_base->SetActionTimeline(action_timeline);
	}

	return new_layer_base;
}

void UI::UILayerAgent::Init()
{

}

void UI::UILayerAgent::Uninit()
{

}

bool UI::UILayerAgent::CheckVisible( UILayerType type )
{
	auto running_scene = UIManager::Instance().scene_agent_.GetCurrSceneBase();
	auto ui_info_iter = ui_info_map_.find(type);
	if (!running_scene || 
		ui_info_iter == ui_info_map_.end())
	{
		return false;
	}

	for (auto node : running_scene->getChildren())
	{
		auto ui_base = dynamic_cast<LayerBase*>(node);
		if (ui_base == nullptr)
		{
			continue;
		}

		if (type == ui_base->GetUILayerType() && ui_base->isVisible())
		{
			return true;
		}
	}

	return false;
}
