#pragma once
#include "AppDefines.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "UIBase.h"

/************************************************************************/
/* type                                                                 */
/************************************************************************/

enum class UISceneType : short
{
	INVALID = -1,
	Start,
	Lobby,
	Load,
	Battle,
	Hello,
	COUNT,
	START = 0,
};

static UISceneType GetUISceneType(std::string value)
{
	if (value == "HelloScene") return UISceneType::Hello;
	if (value == "StartScene") return UISceneType::Start;
	if (value == "LoadScene") return UISceneType::Load;
	if (value == "BattleScene") return UISceneType::Battle;
	if (value == "LobbyScene") return UISceneType::Lobby;

	return UISceneType::INVALID;
}

enum class UILayerType : short
{
	INVALID = -1,
	//Page
	START_Page,
	Home,
	CharacterSelect,
	END_Page,
	//Popup
	START_Popup,
	AttendancePopup,
	END_Popup,
	//Msg
	START_Msg,
	Msg,
	END_Msg,
	//Error	
	COUNT,
	START = 0,
};

enum class UINodeType : short
{
	INVALID = -1,
	Attendance,
	MainTab,
	MainStatus,
	COUNT,
	START = 0,
};

enum class UINotifyType
{
	INVALID = -1,
	ShowScene,
	ShowLayer,
	ShowNode,
	//
	LobbyBottomBtnClicked,
	BackToBeforePage,
	COUNT,
	START = 0,
};

static UILayerType GetUILayerType(std::string value)
{
	if (value == "AttendancePopupLayer") return UILayerType::AttendancePopup;
	else if (value == "LobbyCharacterSelectLayer") return UILayerType::CharacterSelect;
	else if (value == "LobbyHomeLayer") return UILayerType::Home;

	return UILayerType::INVALID;
}
static UINodeType GetUINodeType(std::string value)
{
	return UINodeType::INVALID;
}