#include "ShooterPch.h"
#include "UISceneAgent.h"
#include "Scene/HelloWorldScene.h"
#include "Scene/StartScene.h"
#include "Scene/BattleScene.h"
#include "Scene/LoadScene.h"
#include "Scene/LobbyScene.h"
#include "UIManager.h"
#include "Scene/SceneBase.h"
using namespace UI;

UI::UISceneAgent::UISceneAgent()
	: curr_scene_base_(nullptr)
{

}

UI::UISceneAgent::~UISceneAgent()
{
	while (!ui_info_map_.empty())
	{
		auto iter = ui_info_map_.begin();
		delete iter->second;
		ui_info_map_.erase(iter);
	}
}

void UI::UISceneAgent::Show(UISceneType type)
{
	auto scene_base = this->Create(type);
	auto new_scene = Scene::create();

	curr_scene_base_ = scene_base;
	new_scene->addChild(scene_base);

	Director::getInstance()->replaceScene(new_scene);
}

SceneBase* UI::UISceneAgent::GetCurrSceneBase()
{
	return curr_scene_base_;
}

SceneBase* UI::UISceneAgent::Create(UISceneType type)
{
	Node* node = UIManager::LoadNode(ui_info_map_.at(type));
	CCASSERT(node, "node is null!");

	SceneBase* new_scene_base = dynamic_cast<SceneBase*>(node);

	auto action_timeline = UIManager::LoadActionTimeline(ui_info_map_.at(type));
	if (action_timeline)
	{
		new_scene_base->SetActionTimeline(action_timeline);
	}

	return new_scene_base;
}

