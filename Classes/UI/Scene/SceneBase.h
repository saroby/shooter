#pragma once
#include "../UIDefines.h"

namespace UI
{
	class SceneBase
		: public Scene
		, public UIBase
	{
	public:
		SceneBase(UISceneType type);
		virtual ~SceneBase();

		bool init() override;
		void onEnter() override;

		UISceneType GetUISceneType() { return scene_type_; };
	protected:
		UISceneType scene_type_;
	};
}
