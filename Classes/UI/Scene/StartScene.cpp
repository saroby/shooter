#include "ShooterPch.h"
#include "StartScene.h"
#include "UI/UIManager.h"
#include "cocosbuilder/CocosBuilder.h"

StartSceneReader* StartSceneReader::inst_ = nullptr;

//////////////////////////////////////////////////////////////////////////

UI::StartScene::StartScene()
	: SceneBase(UISceneType::START)
{
	
}

UI::StartScene::~StartScene()
{
	CC_SAFE_RELEASE_NULL(conn_.bg_img_);
	CC_SAFE_RELEASE_NULL(conn_.loadingbar_);
}

bool UI::StartScene::Show()
{
	if (!SceneBase::Show())
	{
		return false;
	}




	cmd::Cmd<UINotifyType> cmd(UINotifyType::ShowLayer);
	cmd.data_stream_->Write(UILayerType::AttendancePopup);
	UIManager::Instance().Broadcast(cmd);

	return true;
}

bool UI::StartScene::init()
{
	if (!SceneBase::init())
	{
		return false;
	}
	return true;
}

void UI::StartScene::onEnter()
{
	SceneBase::onEnter();

	conn_.bg_img_ = static_cast<ui::ImageView*>(getChildByName("Image_1"));
	conn_.loadingbar_ = static_cast<ui::LoadingBar*>(getChildByName("LoadingBar_1"));
	conn_.loadingbar_->setPercent(0);

	auto cmd = Cmd<UINotifyType>(UINotifyType::ShowLayer);
	cmd.data_stream_->Write(UILayerType::Home);
	UIManager::Instance().Broadcast(cmd);
}

void UI::StartScene::onEnterTransitionDidFinish()
{
	SceneBase::onEnterTransitionDidFinish();
	//conn_.bg_img_->setVisible(true);

}
