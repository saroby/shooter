#pragma once
#include "SceneBase.h"

namespace UI
{
	class LobbyScene
		: public SceneBase
	{
	public:
		LobbyScene();
		~LobbyScene();
		CREATE_FUNC(LobbyScene);
	private:

		void onEnter() override;
	};

	GLUE_LOADER(LobbySceneReader, LobbyScene);

}



