#pragma once
#include "SceneBase.h"

namespace UI 
{
	class BattleScene
		: public SceneBase
	{
	public:
		BattleScene();
		~BattleScene();
		CREATE_FUNC(BattleScene);

	private:
	};

	GLUE_LOADER(BattleSceneReader, BattleScene);
}



