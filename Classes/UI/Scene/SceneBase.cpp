#include "ShooterPch.h"
#include "SceneBase.h"

UI::SceneBase::SceneBase(UISceneType type)
	: scene_type_(type)
{

}

UI::SceneBase::~SceneBase()
{

}

bool UI::SceneBase::init()
{
	if (!Scene::init())
	{
		return false;
	}

	return true;
}

void UI::SceneBase::onEnter()
{
	Scene::onEnter();
	Show();
}


