#pragma once
#include "SceneBase.h"

namespace UI
{
	class LoadScene :
		public SceneBase
	{
	public:
		LoadScene();
		~LoadScene();
		CREATE_FUNC(LoadScene);
	};

	GLUE_LOADER(LoadSceneReader, LoadScene);
}



