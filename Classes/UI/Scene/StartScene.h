#pragma once
#include "SceneBase.h"

namespace UI
{
	class StartScene
		: public SceneBase
	{
	public:
		StartScene();
		~StartScene();
		CREATE_FUNC(StartScene);

		bool Show() override;

		bool init() override;
		void onEnter() override;
		void onEnterTransitionDidFinish() override;

	private:
		struct Connector
		{
			ui::ImageView* bg_img_;
			ui::LoadingBar* loadingbar_;

			Connector()
				: bg_img_(nullptr)
				, loadingbar_(nullptr)
			{}
		} conn_;
	};

	GLUE_LOADER(StartSceneReader, StartScene);
}
