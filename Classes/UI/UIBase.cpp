#include "ShooterPch.h"
#include "UIBase.h"

UI::UIBase::UIBase( void )
	: action_timeline_(nullptr)
{
	
}

UI::UIBase::~UIBase( void )
{
	if (action_timeline_)
	{
		action_timeline_->release();
		action_timeline_ = nullptr;
	}
}

bool UI::UIBase::Show()
{
	this->RunAni("show");

	return true;
}

bool UI::UIBase::Hide()
{
	this->RunAni("hide", false, [=](Ref* target) {
		static_cast<Node*>(target)->removeAllChildrenWithCleanup(true);
	});

	return true;
}


bool UI::UIBase::RegisterAniCB(const std::string& ani_key, AniCBFunc end_cb)
{
	if (end_cb == nullptr)
	{
		CC_ASSERT(false);
		return false;
	}
	
	auto ani_cb_iter = ani_end_cb_map_.find(ani_key);

	if (ani_cb_iter != ani_end_cb_map_.end())
	{
		std::vector<AniCBFunc>& cb_list = ani_cb_iter->second;
		for (auto& cb : cb_list)
		{
			if (cb.target<AniCBFunc>() == end_cb.target<AniCBFunc>())
			{
				CCLOG("this is alreay registered in the callback list!");
				return false;
			}
		}

		cb_list.push_back(end_cb);
	}
	else
	{
		std::vector<AniCBFunc> cb_list;
		cb_list.push_back(end_cb);

		ani_end_cb_map_.insert(std::make_pair(ani_key, cb_list));
	}
	
	return true;
}

void UI::UIBase::UnregisterAniCB(const std::string& ani_key, AniCBFunc end_cb)
{
	if (end_cb == nullptr)
	{
		CC_ASSERT(false);
		return;
	}

	auto ani_cb_iter = ani_end_cb_map_.find(ani_key);

	if (ani_cb_iter == ani_end_cb_map_.end())
	{
		return;
	}

	std::vector<AniCBFunc>& cb_list = ani_cb_iter->second;
	
	for (auto& iter = cb_list.begin(); iter != cb_list.end(); iter++)
	{
		if (iter->target<AniCBFunc>() == end_cb.target<AniCBFunc>())
		{
			cb_list.erase(iter);
			break;
		}
	}

	if (cb_list.empty())
	{
		ani_end_cb_map_.erase(ani_key);
	}
}

void UI::UIBase::AniEndCb(const std::string& ani_key)
{
	CCLOG(ani_key.c_str());
}

void UI::UIBase::SetActionTimeline(ActionTimeline* action_timeline)
{
	if (action_timeline_)
	{
		action_timeline_->clearFrameEventCallFunc();
		action_timeline_->release();
	}

	action_timeline_ = action_timeline;

	if (action_timeline_)
	{
		action_timeline_->retain();

		action_timeline_->setLastFrameCallFunc([this]()
		{
			int curr_frame = action_timeline_->getCurrentFrame();
			for (auto ani_cb_iter : ani_end_cb_map_)
			{
				auto frame_info = action_timeline_->getAnimationInfo(ani_cb_iter.first);
				if (curr_frame == frame_info.endIndex)
				{
					std::vector<AniCBFunc> cb_list = ani_cb_iter.second;
					for (auto& cb : cb_list)
					{
						cb(action_timeline_->getTarget());
					}

					AniEndCb(ani_cb_iter.first);
				}
			}
		});

		auto node = dynamic_cast<CCNode*>(this);
		if (node)
		{
			node->runAction(action_timeline_);
		}
	}
}


void UI::UIBase::RunAni(std::string ani_key, bool loop /*= false*/)
{
	if (action_timeline_)
	{
		action_timeline_->play(ani_key, loop);
	}
}

void UI::UIBase::RunAni(std::string ani_key, bool loop, AniCBFunc end_cb /*= nullptr*/)
{
	if (action_timeline_)
	{
		RegisterAniCB(ani_key, end_cb);
		action_timeline_->play(ani_key, loop);
	}
}
