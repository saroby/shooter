#include "ShooterPch.h"
#include "LayerBase.h"
#include "LayerDefines.h"

UI::LayerBase::LayerBase(UILayerType type)
	: layer_type_(type)
{
	
}

UI::LayerBase::~LayerBase()
{
}

bool UI::LayerBase::init()
{
	if (!Layer::init())
	{
		return false;
	}

	return true;
}

void UI::LayerBase::HandleCmd(const cmd::Cmd<UINotifyType>& cmd)
{
	switch (cmd.cmd_type_)
	{
	case UINotifyType::LobbyBottomBtnClicked:
	{
		int tab_type;
		cmd.data_stream_->Read(tab_type);
	}
	break;
	default:
		break;
	}
}

void UI::LayerBase::onEnter()
{
	Layer::onEnter();
	Show();
}


void UI::LayerBase::onExit()
{
	Layer::onExit();
}

