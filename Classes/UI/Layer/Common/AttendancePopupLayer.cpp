#include "ShooterPch.h"
#include "AttendancePopupLayer.h"
#include "cocosbuilder/CocosBuilder.h"

using namespace UI;
using namespace cocosbuilder;
AttendancePopupLayerReader* AttendancePopupLayerReader::inst_ = nullptr;

//////////////////////////////////////////////////////////////////////////

UI::AttendancePopupLayer::AttendancePopupLayer()
	: LayerBase(UILayerType::AttendancePopup)
{

}

UI::AttendancePopupLayer::~AttendancePopupLayer()
{

}

bool UI::AttendancePopupLayer::init()
{
	if (!LayerBase::init())
	{
		return false;
	}

	return true;
}

void UI::AttendancePopupLayer::close(Ref* node)
{
	CCLOG("close btn clicked!");

	Hide();
}

void UI::AttendancePopupLayer::onEnter()
{
	LayerBase::onEnter();
	
	this->conn_.close_btn = static_cast<ui::Button*>(this->getChildByName("close_btn"));
	this->conn_.close_btn->addClickEventListener(std::bind(&AttendancePopupLayer::close, this, std::placeholders::_1));
	this->conn_.main_sprite = static_cast<Sprite*>(this->getChildByName("main_sprite"));

}

//void UI::AttendancePopupLayer::onEnterTransitionDidFinish()
//{
//	LayerBase::onEnterTransitionDidFinish();
//}

