#pragma once
#include "../LayerDefines.h"

namespace UI
{
	class MsgLayer
		: public LayerBase
	{
	public:
		MsgLayer(void);
		virtual ~MsgLayer(void);

		CREATE_FUNC(MsgLayer);
		bool init();
	private:

	};

	GLUE_LOADER(MsgLayerReader, MsgLayer);
}