#include "ShooterPch.h"
#include "MsgLayer.h"

using namespace UI;
MsgLayerReader* MsgLayerReader::inst_ = nullptr;

UI::MsgLayer::MsgLayer( void )
	: LayerBase(UILayerType::Msg)
{

}

UI::MsgLayer::~MsgLayer( void )
{

}

bool UI::MsgLayer::init()
{
	if (!LayerBase::init())
	{
		return false;
	}

	return true;
}
