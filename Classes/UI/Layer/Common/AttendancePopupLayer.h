#pragma once
#include "../LayerBase.h"

namespace UI
{
	class AttendancePopupLayer
		: public LayerBase
	{
	public:
		AttendancePopupLayer();
		~AttendancePopupLayer();
		CREATE_FUNC(AttendancePopupLayer);

		bool init() override;
		void onEnter() override;
		//void onEnterTransitionDidFinish() override;
	private:
		struct Connector
		{
			ui::Button* close_btn;
			Sprite* main_sprite;
			Label* bottom_notice_text;

			Connector()
				: close_btn(nullptr)
				, main_sprite(nullptr)
				, bottom_notice_text(nullptr)
			{

			}
		} conn_;

		void close(Ref* node);
	};

	GLUE_LOADER(AttendancePopupLayerReader, AttendancePopupLayer)
}


