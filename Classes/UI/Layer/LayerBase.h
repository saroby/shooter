#pragma once
#include "LayerDefines.h"
#include "Base/cmd/CmdHandler.h"
#include "cocostudio/WidgetReader/NodeReader/NodeReader.h"
//////////////////////////////////////////////////////////////////////////

namespace UI
{
	class LayerBase
		: public Layer
		, public UIBase
		, protected cmd::CmdHandler<UINotifyType>
	{
	public:
		LayerBase(UILayerType type);
		virtual ~LayerBase();

		//S: Layer
		virtual bool init() override;
		virtual void onEnter() override;
		virtual void onExit() override;
		//E: Layer

		//S: CmdHandler
		virtual void HandleCmd(const cmd::Cmd<UINotifyType>& cmd) override;
		//E: CmdHandler

		UILayerType GetUILayerType() { return layer_type_; };
	protected:
		UILayerType layer_type_;
	};
}

