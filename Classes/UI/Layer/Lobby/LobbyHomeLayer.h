#pragma once
#include "../LayerBase.h"

namespace UI
{
	class LobbyHomeLayer
		: public LayerBase
	{
	public:
		LobbyHomeLayer();
		~LobbyHomeLayer();
		CREATE_FUNC(LobbyHomeLayer);

		void onEnter() override;
		
	private:
		struct Connector
		{

			Connector()
			{

			}
		} conn_;

	};

	GLUE_LOADER(LobbyHomeLayerReader, LobbyHomeLayer)
}
