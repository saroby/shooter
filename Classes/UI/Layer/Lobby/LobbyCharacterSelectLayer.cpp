#include "ShooterPch.h"
#include "LobbyCharacterSelectLayer.h"

using namespace UI;
LobbyCharacterSelectLayerReader* LobbyCharacterSelectLayerReader::inst_ = nullptr;
//////////////////////////////////////////////////////////////////////////

UI::LobbyCharacterSelectLayer::LobbyCharacterSelectLayer()
	: LayerBase(UILayerType::CharacterSelect)
{

}


UI::LobbyCharacterSelectLayer::~LobbyCharacterSelectLayer()
{

}


void UI::LobbyCharacterSelectLayer::onEnter()
{
	LayerBase::onEnter();
}

