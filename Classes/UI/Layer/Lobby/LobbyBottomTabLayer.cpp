#include "ShooterPch.h"
#include "LobbyBottomTabLayer.h"

using namespace UI;
LobbyBottomTabLayerReader* LobbyBottomTabLayerReader::inst_ = nullptr;
//////////////////////////////////////////////////////////////////////////

UI::LobbyBottomTabLayer::LobbyBottomTabLayer( void )
	: LayerBase(UILayerType::AttendancePopup)
{

}

UI::LobbyBottomTabLayer::~LobbyBottomTabLayer( void )
{

}

bool UI::LobbyBottomTabLayer::init()
{
	if (!LayerBase::init())
	{
		return false;
	}

	return true;
}

