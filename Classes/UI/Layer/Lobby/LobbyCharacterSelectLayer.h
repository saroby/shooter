#pragma once
#include "../LayerBase.h"

namespace UI
{
	class LobbyCharacterSelectLayer
		: public LayerBase
	{
	public:
		LobbyCharacterSelectLayer();
		~LobbyCharacterSelectLayer();
		CREATE_FUNC(LobbyCharacterSelectLayer);

		void onEnter() override;

	private:
		struct Connector
		{

			Connector()
			{

			}
		} conn_;

	};

	GLUE_LOADER(LobbyCharacterSelectLayerReader, LobbyCharacterSelectLayer)
}


