#pragma once
#include "../LayerDefines.h"

namespace UI
{
	class LobbyBottomTabLayer
		: public LayerBase
	{
	public:
		LobbyBottomTabLayer(void);
		~LobbyBottomTabLayer(void);
		CREATE_FUNC(LobbyBottomTabLayer);

		bool init() override;
	private:
	};

	GLUE_LOADER(LobbyBottomTabLayerReader, LobbyBottomTabLayer);
}