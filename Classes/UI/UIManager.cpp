#include "ShooterPch.h"
#include "UIManager.h"
#include "Layer/LayerBase.h"
#include "Base/cmd/Cmd.h"
#include "UIBase.h"
#include "Helper/HelperReader.h"

//S: Custom class
#include "Scene/BattleScene.h"
#include "Scene/LoadScene.h"
#include "Scene/LobbyScene.h"
#include "Scene/StartScene.h"
#include "Scene/HelloWorldScene.h"
#include "Layer/Lobby/LobbyBottomTabLayer.h"
#include "Layer/Lobby/LobbyHomeLayer.h"
#include "Layer/Lobby/LobbyCharacterSelectLayer.h"
#include "Layer/Common/MsgLayer.h"
#include "Layer/Common/AttendancePopupLayer.h"
//E: Custom class

using namespace UI;

#define REGISTER_CUSTOM_CLASS(class_name, getter_mothod) \
	CSLoader::getInstance()->registReaderObject(class_name, (ObjectFactory::Instance)getter_mothod);

UI::UIInfo::UIInfo(tinyxml2::XMLElement* element)
{
	this->name_ = element->Attribute("name");
	this->file_ = element->Attribute("file");
	this->z_orer_ = GetZOrder(element->Attribute("z_order"));
	this->type_ = GetType(element->Attribute("type"));
}

//////////////////////////////////////////////////////////////////////////

UI::UIManager::UIManager()
{
	this->Init();
}


UI::UIManager::~UIManager()
{
	this->Uninit();
}

void UI::UIManager::Init()
{
	{
		tinyxml2::XMLDocument doc;

		std::string file_path = cocos2d::FileUtils::getInstance()->fullPathForFilename("Xml/ui.xml");
		cocos2d::log("external file path = %s", file_path.c_str());

		if (doc.LoadFile(file_path.c_str()) != tinyxml2::XML_NO_ERROR)
		{
			cocos2d::log("ui.xml load error!");
			return;
		}

		tinyxml2::XMLElement* root = doc.RootElement();
		tinyxml2::XMLElement* element;

		element = root->FirstChildElement("ui_info_list");
		element = element->FirstChildElement("ui_info");

		while (element)
		{
			UIInfo* ui_info = new UIInfo(element);
			switch (ui_info->type_)
			{
			case UIInfo::Type::Scene:
			{
				auto ui_scene_type = GetUISceneType(ui_info->name_);
				if (ui_scene_type != UISceneType::INVALID)
				{
					scene_agent_.ui_info_map_.insert(std::make_pair(ui_scene_type, ui_info));
				}
			}
			break;
			case UIInfo::Type::Layer:
			{
				auto ui_layer_type = GetUILayerType(ui_info->name_);
				if (ui_layer_type != UILayerType::INVALID)
				{
					layer_agent_.ui_info_map_.insert(std::make_pair(ui_layer_type, ui_info));
				}
			}
			break;
			case UIInfo::Type::Node:
			{
				auto ui_node_type = GetUINodeType(ui_info->name_);
				if (ui_node_type != UINodeType::INVALID)
				{
					node_agent_.ui_info_map_.insert(std::make_pair(ui_node_type, ui_info));
				}
			}
			break;
			default: 
				CCASSERT(false, "not expected ui type!");
				break;
			}
			
			element = element->NextSiblingElement();
		}

		cocos2d::log("ui.xml load success.");
	}
	
	RegisterCustomClass();

	RegisterHandleCmd(&UIManager::Instance(), UINotifyType::ShowScene);
	RegisterHandleCmd(&UIManager::Instance(), UINotifyType::ShowLayer);
}

void UI::UIManager::Uninit()
{
	cocostudio::destroyCocosStudio();
}

void UI::UIManager::RegisterCustomClass()
{
	REGISTER_CUSTOM_CLASS("BattleSceneReader", BattleSceneReader::getInstance);
	REGISTER_CUSTOM_CLASS("LoadSceneReader", LoadSceneReader::getInstance);
	REGISTER_CUSTOM_CLASS("LobbySceneReader", LobbySceneReader::getInstance);
	REGISTER_CUSTOM_CLASS("StartSceneReader", StartSceneReader::getInstance);
	REGISTER_CUSTOM_CLASS("MsgLayerReader", MsgLayerReader::getInstance);
	REGISTER_CUSTOM_CLASS("LobbyBottomTabLayerReader", LobbyBottomTabLayerReader::getInstance);
	REGISTER_CUSTOM_CLASS("AttendancePopupLayerReader", AttendancePopupLayerReader::getInstance);
	REGISTER_CUSTOM_CLASS("LobbyHomeLayerReader", LobbyHomeLayerReader::getInstance);
	REGISTER_CUSTOM_CLASS("LobbyCharacterSelectLayerReader", LobbyCharacterSelectLayerReader::getInstance);
}

Node* UI::UIManager::LoadNode(UIInfo* ui_info)
{
	Node* result = nullptr;

	if (NULL == ui_info)
	{
		CCASSERT(false, "error");
		return result;
	}

	CSLoader* reader = CSLoader::getInstance();
	result = reader->createNode(ui_info->file_.c_str());

	return result;
}


cocostudio::timeline::ActionTimeline* UI::UIManager::LoadActionTimeline(UIInfo* ui_info)
{
	cocostudio::timeline::ActionTimeline* result = nullptr;

	if (NULL == ui_info)
	{
		CCASSERT(false, "error");
		return result;
	}

	CSLoader* reader = CSLoader::getInstance();
	result = reader->createTimeline(ui_info->file_.c_str());

	return result;
}

void UI::UIManager::HandleCmd(const Cmd<UINotifyType>& cmd)
{
	switch (cmd.cmd_type_)
	{
	case UINotifyType::ShowScene:
	{
		UISceneType scene_type;
		cmd.data_stream_->Read(scene_type);
		scene_agent_.Show((UISceneType)scene_type);
	}
	break;
	case UINotifyType::ShowLayer:
	{
		UILayerType layer_type;
		cmd.data_stream_->Read(layer_type);
		layer_agent_.Show((UILayerType)layer_type);
	}
	break;
	case UINotifyType::LobbyBottomBtnClicked:
	{
		int clicked_idx = 0;
	}
	break;
	case UINotifyType::BackToBeforePage:
	{
		if (!CheckPageBackEnable())
		{
			return;
		}

		layer_agent_.Back();
	}
	break;
	default:
	{

	}
	break;
	}
}

bool UI::UIManager::CheckPageBackEnable()
{
	std::vector<UILayerType> disable_layer_type_list;
	
	//S: insert disable layer types

	//E: insert disable layer types

	for (auto layer_type : disable_layer_type_list)
	{
		layer_agent_.CheckVisible(layer_type);
	}

	return true;
}


//bool TryShowPage(UILayerType page);
//bool TryPopPage();
//bool TryShowPopup(UILayerType popup);
//bool TryShowMsg(std::string title, std::string msg);


//bool UI::UIManager::TryShowPage(UILayerType type)
//{
//	if (!page_changable_)
//	{
//		CCLOG(false, "page not changable!");
//		return false;
//	}
//
//	CCASSERT(type > UILayerType::START_Page || type < UILayerType::END_Page, "it's not a page type!");
//	
//	layer_agent_.Show(type);
//	return true;
//}
//
//bool UI::UIManager::TryPopPage()
//{
//	if (!page_changable_)
//	{
//		CCLOG(false, "page not changable!");
//		return false;
//	}
//
//	layer_agent_.Back();
//	
//	return true;
//}
